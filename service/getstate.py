import json
import tangelo
import urllib

tangelo.paths(".")
import utils


def run(statehash, *args, **kwargs):
    response = {}
    statehash = urllib.unquote(statehash)
    coll = utils.getLogDatabase()
    if not coll:
        return ''
    filter = {'reference': statehash}
    result = coll.find_one(filter, {'state': True, '_id': False})
    if result and 'state' in result:
        response['result'] = result['state']

    return json.dumps(response)
