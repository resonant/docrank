import bson.json_util
import json
import pymongo
import tangelo

tangelo.paths(".")
import utils


def run(*args, **kwargs):
    coll = utils.getLogDatabase()
    if not coll:
        return ''
    filter = {}
    params = {}
    pretty = False
    for key in kwargs:
        val = kwargs[key]
        if key.startswith('!'):
            continue
        if key == 'pretty':
            pretty = (val.lower() == 'true')
        elif key in ('skip', 'limit'):
            params[key] = int(val)
        elif key in ('modifiers', 'projection', 'sort', 'fields', 'filter',
                     'spec'):
            if val.startswith('{') or val.startswith('['):
                val = json.loads(val)
            params[key] = val
        elif val.startswith('{') or val.startswith('['):
            filter[key] = json.loads(val)
        else:
            filter[key] = val
    if 'sort' not in params:
        params['sort'] = [('_id', pymongo.DESCENDING)]
    for key in ('spec', 'filter'):
        if key in params:
            filter = params[key]
            del params[key]
    cursor = coll.find(filter, **params)
    result = {'result': [row for row in cursor]}
    result['count'] = cursor.count()
    result['countReturned'] = len(result['result'])
    if pretty:
        jsonparams = {
            'indent': 2,
            'sort_keys': True
        }
    else:
        jsonparams = {
            'separators': (',', ':')
        }
    return bson.json_util.dumps(result, **jsonparams)
