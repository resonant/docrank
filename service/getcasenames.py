import json
import tangelo

tangelo.paths(".")
import elasticsearchutils
# Note that it isn't necessary to say foo = tangelo.local_import('foo'), but
# it keeps pyflakes happy.
# elasticsearchutils = tangelo.import_local('elasticsearchutils')


def run(case):
    cases = elasticsearchutils.getCases(allCases=True)
    caseData = cases.get(case)
    if caseData:
        order = [(
            guid not in caseData.get('used', {}),
            guid not in caseData.get('pa', {}),
            caseData['guids'][guid],
            guid
        ) for guid in caseData['guids']]
        order.sort()
        caseData['order'] = [entry[-1] for entry in order]
    response = {'result': caseData}

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
