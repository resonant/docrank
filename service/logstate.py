import cherrypy
import json
import random
import string
import tangelo

tangelo.paths(".")
import utils


def run(*args, **kwargs):
    data = json.loads(cherrypy.request.body.read().decode('utf8'))
    coll = utils.getLogDatabase()
    if not coll:
        return ''
    while True:
        ref = ''.join(random.SystemRandom().choice(
            string.ascii_letters + string.digits) for _ in range(7))
        if not coll.find_one({'reference': ref}):
            break
    data['reference'] = ref
    coll.save(data)
    return {'reference': data['reference']}
