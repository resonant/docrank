import json
import tangelo

tangelo.paths(".")
import elasticsearchutils


def run():
    # Create an empty response object.
    response = {}

    cases = elasticsearchutils.getCases()
    response['result'] = {'nodes': sorted(cases.keys())}
    allcases = elasticsearchutils.getCases(allCases=True)
    response['result']['allnodes'] = [val[-1] for val in sorted([
        (key not in cases, key + ('' if key in cases else ' *'))
        for key in allcases])]

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
