import elasticsearch
import json
import tangelo
import threading
import unicodedata

tangelo.paths(['.', 'mitll_string_match/src/scripts'])
tangelo.paths('.')
import elasticsearchutils
import utils

try:
    import Levenshtein
except Exception:
    Levenshtein = None

try:
    import softtfidf
    STF = softtfidf.Softtfidf()
    STFLock = threading.Lock()
except Exception:
    STF = None


def calculateMetrics(guid, entities, knownMetrics={}):
    """
    Get the main entity and calculate some simple metrics.  This should be
    replaced by pre-computed data.

    :param guid: PersonGuid of the main entity.
    :param entities: matching entities from the document database.
    """
    collection = utils.getDefaultConfig()[elasticsearchutils.EntitiesDbKey]
    es = elasticsearch.Elasticsearch(collection, timeout=300)
    query = {
        'query': {'function_score': {'query': {'bool': {'must': [
            {'match': {'PersonGUID': guid}},
        ]}}}},
    }
    res = es.search(body=json.dumps(query))
    if not len(res['hits']['hits']):
        return
    doc = res['hits']['hits'][0]['_source']
    names = []
    fullnames = []
    for identIter in xrange(len(doc['Identity'])):
        ident = doc['Identity'][identIter]
        fullnames.extend([normalizeAndLower(fullname['FullName']) for fullname
                          in ident['Name'] if fullname.get('FullName')])
        if 'Email' in ident:
            names.extend([normalizeAndLower(email['Username']) for email in
                          ident['Email'] if email.get('Username')])
    allnames = list(set(names + fullnames))
    for entity in entities:
        if entity['id'].lower() in knownMetrics:
            entity['metrics'] = knownMetrics[entity['id'].lower()].copy()
            # continue
        if entity['id'].split(':')[0] in ('twitter_user', 'web_user'):
            altid = 'name_search:' + entity.get('search_term', '').lower()
            if altid in knownMetrics:
                entity['metrics'] = knownMetrics[altid].copy()
                # continue
        calculateMetricsForEntity(entity, names, fullnames, allnames)


def calculateMetricsForEntity(entity, names, fullnames, allnames):
    """
    Calculate metrics for a specific entity.

    :param entity: the entity to use for calculations.
    :param names: a normalized, lowercase list of screen names for the guid.
    :param fullnames: a normalized, lowercase list of full names for the guid.
    :param allnames: the combined list of names and fullnames.
    """
    entity['metrics'] = metrics = entity.get('metrics', {})
    for key in ['jaro_winkler', 'levenshtein']:
        if key in metrics:
            del metrics[key]
    precalc = metrics.keys()
    if entity.get('name') and len(names):
        metrics['name-substring'] = max([
            substringSimilarity(normalizeAndLower(entity['name']), name)
            for name in names])
        metrics['name-levenshtein'] = max([
            levenshteinSimilarity(normalizeAndLower(entity['name']), name)
            for name in names])
    if entity.get('fullname') and len(fullnames):
        metrics['fullname-substring'] = max([
            substringSimilarity(normalizeAndLower(
                entity['fullname']), name) for name in fullnames])
        metrics['fullname-levenshtein'] = max([
            levenshteinSimilarity(normalizeAndLower(
                entity['fullname']), name) for name in fullnames])
    enames = []
    if entity.get('name'):
        enames.append(normalizeAndLower(entity['name']))
    if entity.get('fullname'):
        enames.append(normalizeAndLower(entity['fullname']))
    if len(enames) and len(allnames):
        metrics['allname-substring'] = 0
        metrics['allname-levenshtein'] = 0
        for ename in enames:
            metrics['allname-substring'] = max(
                metrics['allname-substring'], max([
                    substringSimilarity(ename, name) for name in allnames]))
            metrics['allname-levenshtein'] = max(
                metrics['allname-levenshtein'], max([
                    levenshteinSimilarity(ename, name) for name in allnames]))
            if Levenshtein:
                metrics['jaro-winkler'] = max(
                    metrics.get('jaro-winkler', 0), max([
                        Levenshtein.jaro_winkler(ename, name)
                        for name in allnames]))
            if STF and 'tfidf' not in precalc:
                metrics['tfidf'] = max(
                    metrics.get('tfidf', 0), max([
                        tfidfSimilarity(ename, name) for name in allnames]))
    # This is a fallback that shouldn't be done.
    if 'name_rarity' not in metrics:
        metrics['name_rarity'] = 0.5


# This is a straightforward implementation of a well-known algorithm, and thus
# probably shouldn't be covered by copyright to begin with. But in case it is,
# the author (Magnus Lie Hetland) has, to the extent possible under law,
# dedicated all copyright and related and neighboring rights to this software
# to the public domain worldwide, by distributing it under the CC0 license,
# version 1.0. This software is distributed without any warranty. For more
# information, see <http://creativecommons.org/publicdomain/zero/1.0>
def levenshtein(a, b):
    """
    Calculates the Levenshtein distance between a and b.

    :param a: one string to compare.
    :param b: the second string to compare.
    :returns: the Levenshtein distance between the two strings.
    """
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current = range(n + 1)
    for i in range(1, m + 1):
        previous, current = current, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete = previous[j] + 1, current[j - 1] + 1
            change = previous[j - 1]
            if a[j - 1] != b[i - 1]:
                change = change + 1
            current[j] = min(add, delete, change)


def levenshteinSimilarity(s1, s2):
    """
    Calculate and normalize the Levenshtein metric to a result of 0 (poor) to 1
    (perfect).

    :param s1: the first string
    :param s2: the second string
    :return: the normalized result.  1 is a perfect match.
    """
    totalLen = float(len(s1) + len(s2))
    # The C-module version of Levenshtein is vastly faster
    if Levenshtein is None:
        return (totalLen - levenshtein(s1, s2)) / totalLen
    if isinstance(s1, str):
        s1 = s1.decode('utf8')
    if isinstance(s2, str):
        s2 = s2.decode('utf8')
    return (totalLen - Levenshtein.distance(s1, s2)) / totalLen


def longestCommonSubstring(s1, s2):
    """
    Return the longest common substring between two strings.

    :param s1: the first string
    :param s2: the second string
    :return: the longest common substring.
    """
    if len(s2) > len(s1):
        s1, s2 = s2, s1
    lens2p1 = len(s2) + 1
    for l in xrange(len(s2), 0, -1):
        for s in xrange(lens2p1 - l):
            substr = s2[s: s + l]
            if substr in s1:
                return substr
    return ''


def normalizeAndLower(text):
    """
    Convert some text so that it is normalized and lowercased unicode.

    :param text: the text to alter.
    :returns: the normalized and lower-cased text.
    """
    if isinstance(text, str):
        text = text.decode('utf8')
    text = unicodedata.normalize('NFC', text)
    return text.lower()


def substringSimilarity(s1, s2):
    """
    Determine the longest common substring between two strings and normalize
    the results to a scale of 0 to 1.

    :param s1: the first string
    :param s2: the second string
    :return: the normalized result.  1 is a perfect match.
    """
    return 2.0 * len(longestCommonSubstring(s1, s2)) / (len(s1) + len(s2))


def tfidfSimilarity(s1, s2):
    """
    Compute the MITLL TF-IDF similarity.

    :param s1: the first string
    :param s2: the second string
    :return: the normalized result.  1 is a perfect match.
    """
    with STFLock:
        if s1 not in STF.CORPUS:
            STF.CORPUS.append(s1)
        if s2 not in STF.CORPUS:
            STF.CORPUS.append(s2)
        return (STF.score(s1, s2) + STF.score(s2, s1)) * 0.5


def run(guid, *args, **kwargs):
    # Create an empty response object.
    response = {}

    entities = elasticsearchutils.getEntitiesForGuid(guid)
    entityMetrics = elasticsearchutils.getEntityMetricsForGuid(guid)

    # We have to compute metrics ourselves until we get better data
    calculateMetrics(guid, entities, entityMetrics)

    response['result'] = entities
    # For our results, generate the lineup columns
    elasticsearchutils.lineupFromMetrics(
        response, entities, ['id', 'type', 'description'],
        ['enabled', 'confidence'])

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
