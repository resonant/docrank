import json
import tangelo

tangelo.paths(".")
import elasticsearchutils


def run(guid, entityId, *args, **kwargs):
    # Create an empty response object.
    response = {}

    entities = elasticsearchutils.getEntitiesForGuid(guid)
    entity = None
    for testEntity in entities:
        if testEntity.get('id') == entityId:
            entity = testEntity
            break
    entities = elasticsearchutils.getEntitiesForGuid(guid, entity['query'],
                                                     True)
    for testEntity in entities:
        if testEntity.get('id') == entityId:
            response['entity'] = testEntity
    response['result'] = results = {}
    records = elasticsearchutils.getRankingsForGUID(
        guid, queries=entity.get('query'), filters=entity.get('filter'))
    # Massage the data to a simpler form, add a description, and split to
    # different sources
    for record in records:
        source = record.get('source', 'original')
        if not record.get('description'):
            record['description'] = record.get('document', {}).get(
                'text', 'No description')
        mt_text = record.get('mt_text', record.get('analytics', {}).get(
            'text_english', {}).get('text'))
        if mt_text and mt_text.strip():
            record['orig_description'] = record['description']
            record['description'] = mt_text.strip()
        if source not in results:
            results[source] = {'result': []}
        results[source]['result'].append(record)
    for source in results:
        elasticsearchutils.lineupFromMetrics(
            results[source], results[source]['result'],
            ['id', 'doc_type', 'description'], ['derog'], True)

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
