#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import tangelo

tangelo.paths(".")
import elasticsearchutils


def formatTweet(ent, text):
    """
    Take all the URL information available in a Twitter entities block and
    apply them to the text to have a fully functional set of URLs.

    :param ent: the twitter entities record.
    :param text: the text of the tweet.
    :returns: the tweet with url links.
    """
    url_repl = '<a href="%s">%s</a>'  # % (exp_url, disp_url)
    # % (text,text)
    hash_repl = '<a href="https://twitter.com/hashtag/%s?src=hash">#%s</a>'
    # %(screen_name, screen_name)
    user_repl = '<a href="https://twitter.com/%s">@%s</a>'
    transforms = []

    # Collect transformations to the text
    for url in ent.get('urls', []):
        exp_url = url['expanded_url']
        disp_url = url['display_url']
        replacement = url_repl % (exp_url, disp_url)
        transforms.append((url['indices'], replacement))

    for user_mention in ent.get('user_mentions', []):
        screen_name = user_mention['screen_name']
        replacement = user_repl % (screen_name, screen_name)
        transforms.append((user_mention['indices'], replacement))
    for hashtag in ent.get('hashtags', []):
        hash_text = hashtag['text']
        replacement = hash_repl % (hash_text, hash_text)
        transforms.append((hashtag['indices'], replacement))

    # Apply them in reverse order -- highest first
    transforms.sort(reverse=True)

    for indices, replacement in transforms:
        start, end = indices
        # check that we're not replacing any control characters -- off by 1
        # errors sometimes happen in Twitter's offsets
        to_replace = text[start:end]
        # End of string is not always encoded correctly
        if to_replace == u'\u2026':
            # Add some extra whitespace to pad any nonsense that crops up here
            text = text[:-1] + "   " + replacement
        else:
            # Second edit after an end-of-string problem can still be difficult
            if to_replace.endswith('<'):
                replacement = replacement + " <"
            text = text[:start] + replacement + text[end:]
    return text


def process_QCR_Holding(doc):
    """
    Process a QCR_holding twitter document to extract the formatted tweet,
    geolocation, and created date.
    """
    return process_QCR_holdings(doc)


def process_QCR_holdings(doc):
    """
    Process a QCR_holding twitter document to extract the formatted tweet,
    geolocation, and created date.
    """
    info = {}
    # The source is a gnip record
    source = doc['document']['_source']
    info['text'] = source['body']
    if (info['text'].startswith('RT ') and
            len(info['text'].split(None, 2)) == 3 and source.get('object') and
            source['object'].get('body') and source['object'][
            'body'].startswith(info['text'].split(None, 2)[-1][:-1])):
        info['text'] = (' '.join(info['text'].split(None, 2)[:2]) + ' ' +
                        source['object']['body'])
    info['posted'] = source['postedTime']
    if (source.get('geo') and source['geo'].get('coordinates') and
            len(source['geo']['coordinates']) >= 2):
        info['latitude'] = source['geo']['coordinates'][0]
        info['longitude'] = source['geo']['coordinates'][1]
    info['html'] = formatTweet(source['twitter_entities'], info['text'])
    info['type'] = 'Tweet'
    for media in source.get('twitter_entities', {}).get('media', []):
        if media.get('media_url_https'):
            info['image'] = media['media_url_https']
            break
    return info


def process_tweet(doc):
    """
    Process a tweet (Public API) twitter document to extract the formatted
    tweet, geolocation, and created date.
    """
    info = {}
    source = doc['document']
    info['text'] = source['text']
    info['posted'] = source['created_at']
    if (source.get('coordinates') and
            source['coordinates'].get('coordinates') and
            len(source['coordinates']['coordinates']) >= 2):
        info['latitude'] = source['coordinates']['coordinates'][1]
        info['longitude'] = source['coordinates']['coordinates'][0]
    info['html'] = formatTweet(source['entities'], info['text'])
    info['type'] = 'Tweet'
    for media in doc.get('entities', {}).get('media', []):
        if media.get('media_url_https'):
            info['image'] = media['media_url_https']
            break
    return info


def run(guid, entityId, docId, *args, **kwargs):
    # Create an empty response object.
    response = {}

    entities = elasticsearchutils.getEntitiesForGuid(guid)
    entity = None
    for testEntity in entities:
        if testEntity.get('id') == entityId:
            entity = testEntity
            break
    docInfo = None
    docs = elasticsearchutils.getRankingsForGUID(
        guid, queries=entity.get('query'), filters=entity.get('filter'))
    for testDoc in docs:
        if testDoc.get('id') == docId:
            docInfo = testDoc
            break
    doc = None
    if docInfo:
        if 'query' in docInfo:
            doc = elasticsearchutils.getDocument(queries=docInfo['query'])
        else:
            doc = elasticsearchutils.getDocument(
                doc_guid=docInfo.get('doc_guid'))
    response['result'] = doc
    # Manufacture additional usefull fields for each document type
    if doc:
        func = 'process_' + doc.get('doc_type')
        if func in globals():
            response['document'] = globals()[func](doc)
            mt_text = doc.get('mt_text', doc.get('analytics', {}).get(
                'text_english', {}).get('text'))
            if mt_text and mt_text.strip():
                response['document']['mt_text'] = mt_text
        else:
            print 'Can\'t process document.  No %s function' % func
    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
