import elasticsearch
import json
import tangelo
import threading
import time
import urllib3
import warnings
import _strptime  # noqa  this prevents threading import errors

tangelo.paths(".")
import utils  # noqa

# Suppress elasticsearch warnings
urllib3.disable_warnings()
warnings.simplefilter('ignore', UserWarning)


EntitiesDbKey = 'entities'
DocumentDbKey = 'documents'
NameRankingsDbKey = 'nameRankings'
NewEntitiesIndexKey = 'newEntityIndex'

ColumnLabels = {
    'doc_type': 'Type',
    'doc_id': 'Document ID',
    'document': 'Document',
    'desc': 'Description',

    'type': 'Type',
    'description': 'Description',
    'enabled': 'Possible',
    'confidence': 'Confidence',
    'derog': 'Derogatory'
}
ColumnDomains = {
    'confidence': [0, 100],
}
ColumnTypes = {
    'confidence': 'number',
}
ColumnWidths = {
    'description': 140,
}

MetricFilter = {'bool': {'should': [
    {'exists': {'field': 'metrics'}},
    {'bool': {'must': [
        {'term': {'doc_type': 'HG_Profiler'}},
        {'exists': {'field': 'document.category'}},
    ]}},
]}}

CachedResults = {}
CachedResultsLock = threading.Lock()


def cacheResults(dbname, key, value=None, weight=1):
    """
    Store results in our result cache.

    :param dbname: database access key.
    :param key: data query key.
    :param value: value to store.  If None, retreive if available.
    :param weight: when deciding which parts of the cache to discard, higher
                   weights are kept longer.
    :return: the cached value or None.
    """
    with CachedResultsLock:
        if dbname not in CachedResults:
            CachedResults[dbname] = {}
        if value is not None:
            curtime = time.time()
            CachedResults[dbname][key] = {
                'created': curtime,
                'used': curtime,
                'data': value,
                'weight': weight
            }
            while len(CachedResults[dbname]) > 100:
                oldest = key
                oldage = 0
                for subkey in CachedResults[dbname]:
                    age = ((curtime - CachedResults[dbname][subkey]['used']) /
                           CachedResults[dbname][subkey]['weight'])
                    if age > oldage:
                        oldage = age
                        oldest = subkey
                del CachedResults[dbname][oldest]
        if key not in CachedResults[dbname]:
            return None
        CachedResults[dbname][key]['used'] = time.time()
        return CachedResults[dbname][key]['data']


def clearCache(dbname=None):
    """
    Clear the result cache.

    :param dbname: if specified, only clear the result cache reslated to the
                   specific database access key.  Otherwise, clear everything.
    """
    with CachedResultsLock:
        keys = [key for key in CachedResults
                if dbname is None or dbname == key]
        for key in keys:
                del CachedResults[key]


def getCases(allowBuffered=True, allCases=False):
    """
    Get a list of cases that have some document-related data.  For each case,
    get a list of known PersonGUIDs and indicated if these are PA and/or used
    (have document data).

    :param allowBuffered: if True, allow cached results to be used.
    :param allCases: if True, keep all cases, even ones we don't think are
                     used.
    :returns cases: a dictionary.  Each key is a case label.  Each value is a
                    a dict of 'guids', 'used', and 'pa', each of which have
                    keys indicated PersonGUIDs in that category.  The guids
                    also has a string of the form guid - name ... - email ...
                    that can be shown to the user.
    """
    cachekey = 'allcases' if allCases else 'cases'
    dbname = utils.getDefaultConfig()[EntitiesDbKey]
    if allowBuffered and cacheResults(dbname, cachekey):
        return cacheResults(dbname, cachekey)
    dbkey = DocumentDbKey
    esguid = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                         timeout=300)
    usedGuids = getUsedPersonGuids()
    tangelo.log('Number of used PersonGUIDs: %d' % len(usedGuids))
    es = elasticsearch.Elasticsearch(dbname, timeout=300)
    cases = {}
    batch = 10000
    offset = 0
    while True:
        query = {
            '_source': {'include': [
                'PersonGUID', 'Identity.Name.FullName',
                'Identity.Email.OriginalEmail', 'Identity.Payload',
            ]},
            'size': 25000,
            'query': {'function_score': {'filter': {'bool': {'must': [
                {'exists': {'field': 'PersonGUID'}}
            ]}}}},
            'from': offset,
            'size': batch,
            'sort': {'PersonGUID': 'asc'},
        }
        offset += batch
        tangelo.log('getCases ' + dbname + ' ' + json.dumps(query))
        res = es.search(body=json.dumps(query))
        if not len(res.get('hits', {}).get('hits', [])):
            break
        for record in res['hits']['hits']:
            getCasesProcess(record, usedGuids, cases, esguid)
    if not allCases:
        for case in cases.keys():
            if not len(cases[case]['used']):
                del cases[case]
    cacheResults(dbname, cachekey, cases)
    tangelo.log('Number of cases %s: %d' % (
        'with information' if not allCases else '(all)', len(cases)))
    return cases


def getCasesProcess(record, usedGuids, cases, esguid):
    """
    Process information about a case to determine if we have any data regarding
    the various entities.

    :param record: the record to process.
    :param usedGuids: a dictionary of known person guids.
    :param cases: a dictionary to store results.
    :param esguid: an elasticsearch connection to use to ensure guids are used.
    """
    doc = record['_source']
    guid = doc['PersonGUID']
    pa = False
    case = None
    name = [guid]
    for identIter in xrange(len(doc['Identity'])):
        ident = doc['Identity'][identIter]
        for payIter in xrange(len(ident.get('Payload', []))):
            payload = ident['Payload'][payIter]
            key = payload.get('PayloadName', '').upper()
            value = payload.get('PayloadValue')
            if key in ('CASE NUMBER', 'CASE_NUMBER'):
                case = value
            elif key == 'RELATIONSHIP' and value == 'PA':
                pa = True
        name.extend([fullname.get('FullName')
                     for fullname in ident['Name']])
        if 'Email' in ident:
            name.extend([email.get('OriginalEmail')
                         for email in ident['Email']])
    namestr = ' - '.join([subname for subname in name if subname])
    doc['namestr'] = namestr
    if not case:
        return
    if case not in cases:
        cases[case] = {'pa': {}, 'used': {}, 'guids': {}}
    if usedGuids and guid in usedGuids:
        cases[case]['used'][guid] = True
    elif (guid not in cases[case]['used'] and
            all([part in usedGuids for part in guid.split('-')])):
        # If the visa_guid field in ES is analyzed, we have to check to make
        # sure a guid is actually used.  The correct way to do this (other then
        # by fixing the mapping to not analyze that field), is to check the
        # guid explicitly using a query.  However this is slow, so we can trust
        # that there are few guid collisions instead.
        if False:
            query = {
                'size': 0,
                'filter': MetricFilter,
                'query': {'match': {'visa_guid': {
                    'operator': 'and', 'query': guid}}},
            }
            # tangelo.log('getCasesProcess ' + json.dumps(query))
            guidres = esguid.search(body=json.dumps(query))
            if guidres['hits']['total'] > 0:
                cases[case]['used'][guid] = usedGuids[guid] = True
        else:
            cases[case]['used'][guid] = usedGuids[guid] = True
    cases[case]['guids'][guid] = doc['namestr']
    if pa:
        cases[case]['pa'][guid] = True


def getDocument(doc_guid=None, queries=None, filters=None):
    """
    Get a document by doc_guid or by particular queries.

    :param doc_guid: the doc_guid, if known.
    :param queries: a list of additional query specifications to add to the
                    elasticseach query.
    :param filters: a list of additional filter specifications to add to the
                    elasticseach query.
    :returns: the document found.
    """
    dbkey = DocumentDbKey
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    queryList = []
    filterList = []
    if doc_guid is not None:
        # We had been using the doc_guid, but that isn't reliable, so just use
        # use the _id.
        #   queryList.append({'match': {'doc_guid': doc_guid}})
        queryList.append({'match': {'_id': {
            'operator': 'and', 'query': doc_guid}}})
    if queries is not None and len(queries):
        queryList.append(queries)
    if filters is not None and len(filters):
        filterList.append(filters)
    query = {
        'size': 1,
        'query': {'function_score': {
        }},
    }
    if len(filterList):
        query['query']['function_score']['filter'] = {
            'bool': {'must': filterList}}
    if len(queryList):
        query['query']['function_score']['query'] = {
            'bool': {'must': queryList}}
    tangelo.log('getDocument ' + utils.getDefaultConfig()[dbkey] + ' ' +
                json.dumps(query))
    res = es.search(body=json.dumps(query))
    for hit in res['hits']['hits']:
        return hit['_source']
    return None


def getEntitiesForGuid(guid, addQuery=None, moreInfo=False):
    """
    Get a list of entities that might be the same as the specified guid.  Each
    entity has some standardized information, as well as metrics for how well
    they match the guid.

    :param guid: the PersonGuid to match.
    :param addQuery: if present, filter the results with this additional query.
    :param moreInfo: if True allow information that could be deemed derogatory.
    :returns: a list of entities.
    """
    entities = {}
    dbkey = DocumentDbKey
    cachekey = 'getEntitiesForGuid:' + guid
    if addQuery is not None:
        cachekey += ':' + repr(addQuery)
    if moreInfo:
        cachekey += ':more'
    if cacheResults(dbkey, cachekey):
        return cacheResults(dbkey, cachekey)
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    # We get all relevant documents, and extract unique users from that.
    query = {
        '_source': {'include': [
            'doc_type', 'document.user', 'document._source.actor',
            'document.username', 'document.site', 'document.category',
            'document.url', 'document.name', 'doc_guid',
            'document.search_term',
        ]},
        'size': 25000,
        'query': {'function_score': {
            'filter': MetricFilter,
            'query': {'bool': {'must': [
                {'match': {'visa_guid': {'operator': 'and', 'query': guid}}},
            ]}},
        }},
    }
    if addQuery is not None:
        query['query']['function_score']['query']['bool']['must'].append(
            addQuery)
        del query['_source']
    tangelo.log('getEntitiesForGuid ' + utils.getDefaultConfig()[dbkey] + ' ' +
                json.dumps(query))
    res = es.search(body=json.dumps(query))
    tweetTypes = ('tweet', 'Tweet', 'QCR_holdings', 'QCR_Holding')
    for hit in res['hits']['hits']:
        record = hit['_source']
        entity = {}
        docType = record.get('doc_type', hit.get('_type'))
        if docType in tweetTypes:
            entity = getEntityFromTweet(record, tweetTypes, addQuery)
        elif docType == 'Child_Exploitation':
            entity = getEntityFromCEOS(record, moreInfo)
        elif docType == 'HG_Profiler':
            entity = getEntityFromHGProfiler(record, moreInfo, hit)
        # Copy user metrics here, plus any other data we want to show
        if not entity or not entity.get('id'):
            continue
        entities[entity['id']] = entity
    results = entities.values()
    cacheResults(dbkey, cachekey, results, 10 if addQuery is None else 1)
    return results


def getEntityFromCEOS(record, moreInfo):
    """
    Extract entity information from CEOS information.

    :param record: the CEOS record.
    :param moreInfo: if True allow information that could be deemed derogatory.
    :returns: the extracted entity.
    """
    entity = {}
    entity['type'] = 'web_user'
    entity['user_id'] = record['document']['username']
    entity['id'] = entity['type'] + ':' + entity['user_id']
    entity['query'] = [
        {'match': {'doc_type': {
            'operator': 'and', 'query': record.get('doc_type')}}},
        {'match': {'document.username': {
            'operator': 'and', 'query': entity['user_id']}}},
    ]
    entity['description'] = record['document']['username']
    entity['name'] = record['document']['username']
    if moreInfo and record['document'].get('site'):
        desc = record['document'].get('site', {}).get('name')
        if desc and record['document']['site'].get('host'):
            desc += ' (%s)' % record['document']['site']['host']
        else:
            desc = record['document'].get('site', {}).get('host')
        if desc:
            entity['description'] = 'Child Exploitation Memex Record: ' + desc
    return entity


def getEntityFromHGProfiler(record, moreInfo, hit):
    """
    Extract entity information from HG Profiler information.

    :param record: the HG Profiler record.
    :param moreInfo: if True allow information that could be deemed derogatory.
    :param hit: the parent of the record.
    :returns: the extracted entity.
    """
    entity = {}
    entity['type'] = 'web_user'
    username = None
    for doc in record['document']:
        cat = doc.get('name')
        url = doc.get('url', '')
        if cat in ('Blogspot', 'Buzznet', 'DeviantArt', 'Flavors', 'HubPages',
                   'Muzy', 'PhotoBucket', 'SmugMug'):
            username = url.split('//')[1].split('.')[0]
        elif cat in ('FFFFOUND!', 'IFTTT', 'SourceForge', 'YouTube', 'aNobil',
                     'sporcle'):
            username = url.rstrip('/').rsplit('/')[-2]
        else:
            username = url.rstrip('/').rsplit('/', 1)[-1].lstrip('@~')
        if username.endswith('.html'):
            username = username[:-5]
        username = username.split('=')[-1]
        if username:
            break
    if not username:
        return None
    entity['user_id'] = username
    entity['id'] = entity['type'] + ':' + entity['user_id']
    entity['query'] = [
        {'match': {'_id': {'operator': 'and', 'query': hit['_id']}}},
    ]
    entity['description'] = username
    entity['name'] = username
    if moreInfo:
        fulldesc = []
        for doc in record['document']:
            desc = ''
            if doc.get('name'):
                desc = doc['name']
            if doc.get('url'):
                if desc:
                    desc += ' - '
                desc += '<a href="%s">%s</a>' % (doc['url'], doc['url'])
            if desc:
                fulldesc.append(desc)
        if len(fulldesc):
            entity['description'] = '<br/>'.join(fulldesc)
    return entity


def getEntityFromTweet(record, tweetTypes, addQuery):
    """
    Extract entity information from tweet information.

    :param record: the tweet record.
    :param tweetTypes: a list of types that might need to be matched in the
                       database.
    :param addQuery: if present, add filtering information to the result.
    :returns: the extracted entity.
    """
    entity = {}
    entity['type'] = 'twitter_user'
    user = record['document']['user']
    entity['user_id'] = user['screen_name']
    entity['id'] = entity['type'] + ':' + entity['user_id']
    entity['query'] = [
        {'bool': {'should': [
            {'match': {'doc_type': {'operator': 'and', 'query': tweetType}}}
            for tweetType in tweetTypes
        ]}},
        {'match': {'document.user.screen_name': {
            'operator': 'and', 'query': user['screen_name']}}},
    ]
    entity['description'] = '@' + user['screen_name']
    entity['name'] = user['screen_name']
    fullname = user.get('name')
    if not fullname and 'actor' in record['document'].get(
            '_source', {}):
        fullname = record['document']['_source']['actor'].get(
            'displayName')
    if fullname:
        entity['description'] += ' (%s)' % fullname
        entity['fullname'] = fullname
    if addQuery is not None:
        if 'actor' in record['document'].get('_source', {}):
            entity['match'] = record['document']['_source']['actor']
    if 'actor' in record['document'].get('_source', {}):
        if (record['document']['_source']['actor'].get('image') and
                '/default_profile_images/' not in record['document'][
                '_source']['actor']['image']):
            entity['image'] = record['document']['_source']['actor']['image']
    elif ('user' in record['document'] and
            'profile_image_url' in record['document']['user'] and
            '/default_profile_images/' not in record['document']['user'][
            'profile_image_url']):
        entity['image'] = record['document']['user']['profile_image_url']
    if '_normal.' in entity.get('image', ''):
        entity['image'] = entity['image'].replace('_normal.', '.')
    if 'search_term' in record['document']:
        entity['search_term'] = record['document']['search_term']
    return entity


def getEntityMetricsForGuid(guid):
    """
    Get a list of entities that might be the same as the specified guid, each
    with associated metrics.

    :param guid: the PersonGuid to match.
    :returns: a dictionary of entities with metrics.
    """
    entities = {}
    # We are using this one
    dbkey = NameRankingsDbKey
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    # We get all relevant documents, and extract unique users from that.
    query = {
        'size': 25000,
        'query': {'function_score': {
            'filter': {'bool': {'must': [
                # {'exists': {'field': 'matches'}}
            ]}},
            'query': {'bool': {'must': [
                {'match': {'PersonGUID': {'operator': 'and', 'query': guid}}},
            ]}},
        }},
    }
    tangelo.log('getEntityMetricsForGuid ' + utils.getDefaultConfig()[dbkey] +
                ' ' + json.dumps(query))
    res = es.search(body=json.dumps(query))
    entities = {}
    for hit in res['hits']['hits']:
        record = hit['_source']
        for match in record.get('matches', []):
            if 'metrics' not in match:
                continue
            for key in match['metrics'].keys():
                try:
                    match['metrics'][key] = float(match['metrics'][key])
                except ValueError:
                    del match['metrics'][key]
            if not len(match['metrics']):
                continue
            screenname = match.get('screenname', match.get(
                'search_term', '').lower())
            if not screenname:
                continue
            id = record.get('entity_type', 'unknown') + ':' + screenname
            id = id.lower()
            if id not in entities:
                entities[id] = {}
            entities[id].update(match['metrics'])
    return entities


def getMetricDomains(docs, settings={}):
    """
    Based on a set of documents that contain a metrics key, determine the
    domain of unique metrics.

    :param docs: a list of document objects.
    :param settings: a dictionary of metric settings.
    :returns: a dictionary of metric domains.
    """
    metrics = {}
    for doc in docs:
        if 'metrics' not in doc:
            continue
        for metric in doc['metrics'].keys():
            if (settings.get(metric, {}).get('alt') and
                    settings.get(metric, {}).get('alt') not in doc['metrics']):
                altmetric = settings.get(metric, {})['alt']
                doc['metrics'][altmetric] = doc['metrics'][metric]
        for metric in doc['metrics'].keys():
            if not isinstance(doc['metrics'][metric], (int, float)):
                try:
                    doc['metrics'][metric] = float(
                        doc['metrics'][metric])
                except ValueError:
                    del doc['metrics'][metric]
                    continue
                except TypeError:
                    del doc['metrics'][metric]
                    continue
            if metric not in metrics:
                metrics[metric] = [
                    doc['metrics'][metric], doc['metrics'][metric]]
            metrics[metric][0] = min(metrics[metric][0],
                                     doc['metrics'][metric])
            metrics[metric][1] = max(metrics[metric][1],
                                     doc['metrics'][metric])
            # Flatten key structure
            doc[metric] = doc['metrics'][metric]
    return metrics


def getRankingsForGUID(guid, limited=False, queryinfo={}, queries=None,
                       filters=None):
    """
    Get all document rankings associated with a specific guid, or one of each
    ranking type.

    :param guid: the userID for the rankings.
    :param limited: if True, get one of each distinct ranking type.
    :param queryinfo: a dictionary to store query information for different
                      data sources.
    :param queries: a list of additional query specifications to add to the
                    elasticseach query.
    :param filters: a list of additional filter specifications to add to the
                    elasticseach query.
    :returns: records found.
    """
    results = []
    found = {}
    # Only use IST data
    dbkey = DocumentDbKey
    cachekey = 'getRankingsForGUID:' + guid
    queryList = [{'match': {'visa_guid': {'operator': 'and', 'query': guid}}}]
    filterList = [MetricFilter]
    if queries is not None and len(queries):
        queryList.append(queries)
        cachekey += ':q:' + repr(queries)
    if filters is not None and len(filters):
        filterList.append(filters)
        cachekey += ':f:' + repr(queries)
    if cacheResults(dbkey, cachekey):
        return cacheResults(dbkey, cachekey)
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    query = {
        '_source': {'include': [
            'doc_type', 'doc_guid', 'document.user', 'document.id',
            'document.text', 'document.site', 'metrics', 'document.match_type',
            'mt_text', 'analytics.text_english.text'
        ]},
        'size': 25000,
        'query': {'function_score': {
            'filter': {'bool': {'must': filterList}},
            'query': {'bool': {'must': queryList}},
        }},
    }
    queryinfo[dbkey] = query
    tangelo.log('getRankingsForGUID ' + utils.getDefaultConfig()[dbkey] + ' ' +
                json.dumps(query))
    res = es.search(body=json.dumps(query))
    for hit in res['hits']['hits']:
        record = hit['_source']
        newVal = used = False
        for metric in record.get('metrics', {}):
            if metric not in found:
                newVal = True
                found[metric] = True
            used = True
        if used and (not limited or newVal):
            record['db_key'] = dbkey
            # We had been using the doc_guid, but that isn't reliable, so just
            # use the _id.
            record['doc_guid'] = hit['_id']
            record['query'] = [
                {'match': {'_id': {'operator': 'and', 'query': hit['_id']}}},
            ]
            record['source'] = 'original'
            doc = record.get('document', {})
            desc = doc.get('text')
            if desc is None:
                record['source'] = 'user'
                # This needs to be rolled into user information.
                desc = doc.get('site', {}).get('name')
                if desc and doc['site'].get('host'):
                    desc += ' (%s)' % doc['site']['host']
            elif desc.startswith('RT '):
                record['source'] = 'repeat'
            # Add handling of 'mention' sources as well.
            if doc.get('match_type') in ('tweet_text_match', 'other_match'):
                record['source'] = 'mention'
            record['description'] = (
                desc if desc is not None else 'No description')
            record['id'] = record.get('doc_type', '') + ':' + hit['_id']
            results.append(record)
    cacheResults(dbkey, cachekey, results, 1)
    return results


def getUsedPersonGuids(allowBuffered=True):
    """
    Get a dictionary whose keys are the PersonGUIDs for which we have any
    document ranks.

    :param allowBuffered: if True, allow cached results to be used.
    :returns: a dictionary where any key is a PersonGUID for which at least
              one document exists that we have data for.
    """
    dbkey = DocumentDbKey
    if allowBuffered and cacheResults(dbkey, 'usedGuids'):
        return cacheResults(dbkey, 'usedGuids')
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    query = {
        'size': 0,
        'query': {'function_score': {
            'filter': MetricFilter,
        }},
        'aggs': {'guids': {
            'terms': {'field': 'visa_guid', 'size': 100000},
        }},
    }
    tangelo.log('getUsedPersonGuids ' + utils.getDefaultConfig()[dbkey] + ' ' +
                json.dumps(query))
    res = es.search(body=json.dumps(query))
    guids = {}
    for bucket in res['aggregations']['guids']['buckets']:
        guids[bucket['key']] = True
    cacheResults(dbkey, 'usedGuids', guids, 1000)
    return guids


def getUserRankingsForGUID(handle, limited=False, queries=None, filters=None):
    """
    Get all user rankings associated with a specific handle.

    :param handle: the userID for the rankings.
    :param queries: a list of additional query specifications to add to the
                    elasticseach query.
    :param filters: a list of additional filter specifications to add to the
                    elasticseach query.
    :returns: records found.
    """
    dbkey = 'userRankings'
    es = elasticsearch.Elasticsearch(utils.getDefaultConfig()[dbkey],
                                     timeout=300)
    queryList = [{'match': {'PersonGUID': {
        'operator': 'and', 'query': handle}}}]
    filterList = []  # {'exists': {'field': 'metrics'}}]
    if queries is not None and len(queries):
        queryList.append(queries)
    if filters is not None and len(filters):
        filterList.append(filters)
    query = {
        'size': 25000,
        'query': {'function_score': {
            'filter': {'bool': {'must': filterList}},
            'query': {'bool': {'must': queryList}},
        }},
    }
    tangelo.log('getUserRankingsForGUID ' + utils.getDefaultConfig()[dbkey] +
                ' ' + json.dumps(query))
    res = es.search(body=json.dumps(query))
    return [record['_source'] for record in res['hits']['hits']
            if 'metrics' in record['_source']]


def lineupFromMetrics(response, docs, firstColumns, lastColumns=[],
                      includeZeroMetrics=False):
    """
    Add information for lineup into a response document.

    :param response: the dictionary to add lineup data into.  Modified.
    :param docs: a list of documents that contain metrics.
    :param firstColumns: a list of column ids to include at the beginning of
                         the line up.  The first value is used as the unique
                         row id, not as a column.  Required.
    :param lastColumns: a list of column ids to include at the end of the line
                        up.  Optional.
    :param includeZeroMetrics: if True, include metrics for which all values
                               are exactly zero.
    """
    settings = utils.getNamedConfig('metrics.json')
    response['primaryKey'] = firstColumns[0]
    response['columns'] = col = []
    laycol = []
    primecol = []
    response['layout'] = {'primary': primecol}
    for key in firstColumns + lastColumns:
        colData = {
            'column': key,
            'type': ColumnTypes.get(key, 'string'),
        }
        colData['label'] = ColumnLabels.get(key)
        colData['domain'] = ColumnDomains.get(key)
        col.append(colData)
        if len(col) > 1:
            primecol.append({
                'column': key,
                'width': ColumnWidths.get(key, 60)
            })
        else:
            primecol.append({"type": "rank", "width": 5})
        if len(col) == len(firstColumns):
            primecol.append(
                {"type": "stacked", "label": "Combined", "children": laycol})
    metrics = getMetricDomains(docs, settings)
    for metric in metrics.keys():
        domain = metrics[metric]
        if domain[0] >= 0 and domain[1] >= 0:
            domain[0] = 0
            if domain[1] < 1:
                domain[1] = 1
        elif domain[0] < 0 and domain[1] < 0:
            domain[1] = 0
        if (domain[0] == domain[1] and (
                domain[0] != 0 or not includeZeroMetrics)):
            del metrics[metric]
            continue
        if (settings.get(metric, {}).get('hide') or
                settings.get(metric, {}).get('alt')):
            del metrics[metric]
            continue
    for metric in sorted(metrics.keys()):
        col.append({
            'column': metric,
            'type': 'number',
            'domain': metrics[metric],
        })
        if metric in settings:
            for key in settings[metric]:
                col[-1][key] = settings[metric][key]
        laycol.append({'column': metric, 'width': 350./len(metrics)})
