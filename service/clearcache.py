import tangelo

tangelo.paths(".")
import elasticsearchutils


def run(**kwargs):
    """
    Clear the result cache, optionally reloading the case list.  The query can
    accept the following parameters:
     dbname: database access key to clear.  Unspecified for all.
     reload: if 'true', reload the case list.
    """
    elasticsearchutils.clearCache(kwargs.get('dbname'))
    if kwargs.get('reload') == 'true':
        elasticsearchutils.getCases()
        elasticsearchutils.getCases(allCases=True)
    return {'results': 'done'}
