import datetime
import elasticsearch
import json
import tangelo

tangelo.paths(".")
import elasticsearchutils
import utils


def run(handle):
    collection = utils.getDefaultConfig()[elasticsearchutils.EntitiesDbKey]
    es = elasticsearch.Elasticsearch(collection, timeout=300)
    query = {
        'query': {'function_score': {'query': {'bool': {'must': [
            {'match': {'PersonGUID': {'operator': 'and', 'query': handle}}},
        ]}}}},
    }
    tangelo.log('loadentityrecord ' + collection + ' ' + json.dumps(query))
    res = es.search(body=json.dumps(query))
    doc = res['hits']['hits'][0]['_source']
    try:
        if len(doc.get('Identity', [{}])[0].get('FullBirthDate', '')) == 8:
            now = datetime.datetime.now()
            birth = datetime.datetime.strptime(
                doc['Identity'][0]['FullBirthDate'], '%Y%m%d')
            age = now.year - birth.year + (
                -1 if (now.month, now.day) < (birth.month, birth.day) else 0)
            if age >= 0:
                doc['Identity'][0]['Age'] = age
    except (IndexError, ValueError, KeyError):
        pass
    response = {'result': doc}

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
