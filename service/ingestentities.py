#!/usr/bin/env python
"""
Ingest entities from an XML file to Elasticsearch.
"""

import elasticsearch
import json
import sys
import xmltodict
try:
    import tangelo
    tangelo.paths(".")
    import elasticsearchutils
    import utils
except Exception:
    tangelo = None


ListNames = ['Name', 'RetentionMetaData', 'Phone', 'Address', 'Document',
             'Payload', 'Identity', 'Email']


def delete_index(db=None, index=None):
    """
    Delete an elasticsearch index.

    :param db: the name of the db, excluding any indices.
    :param index: the name of the index to delete.
    """
    if not db or not index:
        return
    es = elasticsearch.Elasticsearch(db)
    try:
        es.indices.delete(index=index, timeout=300)
    except elasticsearch.TransportError:
        pass


def ingest_xml(xmlfile, db, index, listindex, results=None, status=None,
               filename=None):
    """
    Ingest xml from a file-like object into the specified database.

    :param db: the url of the database to connect to.  This should not have
               the name of the index attached to it.
    :param index: the name of the index to ingest from.
    :param listindex: the url of the database and indices to query for existing
                      guids.  If None, don't query existing guids.
    :param results: a dictionary to store results and pass to the status
                    callback.
    :param status: if present, a callback to call during partial results.
    :param filename: if present, store the file name in new entries.
    :return: a results dictionary describing what occurred.
    """
    guids = known_guids(listindex)
    if results is None:
        results = {}
    results['existing_guids'] = len(guids)
    results.setdefault('nocase', 0)
    results.setdefault('present', 0)
    results.setdefault('processed', 0)
    results.setdefault('cases', {})
    results.setdefault('guids', {})
    xml = xmltodict.parse(xmlfile)
    data = [xml_to_object(person) for person in xml['Ident']['Person']]
    es = elasticsearch.Elasticsearch(db)
    for entry in data:
        if not len(entry.get('Identity', [])):
            continue
        if entry['PersonGUID'] in guids:
            results['present'] += 1
            continue
        if filename:
            entry['source_file'] = filename.replace('\\', '/').split('/')[-1]
        casenum = None
        for pay in entry['Identity'][0].get('Payload', []):
            key = pay.get('PayloadName', '').upper()
            if key.endswith(': FT Related To'.upper()):
                pay['PayloadName'] = key = key.rsplit(':', 1)[0]
            if key == 'Case Number'.upper():
                pay['PayloadName'] = key = 'CASE_NUMBER'
            if key in ['CASE NUMBER', 'CASE_NUMBER']:
                casenum = pay['PayloadValue']
        if casenum is None:
            results['nocase'] += 1
            continue
        results['guids'][entry['PersonGUID']] = True
        results['cases'][casenum] = True
        results['processed'] += 1
        if status:
            status(results)
        es.index(index=index, doc_type='entity', body=json.dumps(entry),
                 refresh=True)
    return results


def known_guids(dbname=None):
    """
    Return a dictionary of known guids.

    :param dbname: if specified, use this name instead of the setting.
    :returns: a dictionary where the keys are the known guids.
    """
    if not dbname:
        return {}
    es = elasticsearch.Elasticsearch(dbname, timeout=300)
    query = {
        '_source': {'include': ['PersonGUID']},
        'size': 1000000
    }
    res = es.search(body=json.dumps(query))
    guids = {}
    for record in res['hits']['hits']:
        guids[record['_source']['PersonGUID']] = True
    return guids


def run(**kwargs):
    """
    Process the endpoint.  This handles the following options:
     db=(url of the database to use) - defaults to the root url in the
        EntitiesDbKey of the configuration file.
     delete=(true|false) - if true, empty the index before adding data.
     index=(name of the index).  Default is the value in NewEntitiesIndexKey in
        the configuration file.
     listindex=(url of the database to use to list guids).  Default is the
        value in EntitiesDbKey.
     name=(file name to store in new entries)
    """
    if 'db' in kwargs:
        db = kwargs['db'].rstrip('/')
        if not db.startswith('http'):
            db = 'http://' + db
    else:
        db = utils.getDefaultConfig()[elasticsearchutils.EntitiesDbKey]
        if ':' in db and '/' in db[db.index(':'):]:
            db = db[:db.index('/', db.index(':'))]
        elif '/' in db:
            db = db[:db.index('/')]
    index = kwargs.get('index', utils.getDefaultConfig()[
        elasticsearchutils.NewEntitiesIndexKey])
    listindex = kwargs.get('listindex', utils.getDefaultConfig()[
        elasticsearchutils.EntitiesDbKey])
    if kwargs.get('delete') == 'true':
        delete_index(db, index)
    xmlfile = tangelo.request_body()
    status = show_status if kwargs.get('status') == 'true' else None
    results = ingest_xml(xmlfile, db, index, listindex, status=status,
                         filename=kwargs.get('name'))
    results = results.copy()
    results['guids'] = sorted(results['guids'].keys())
    results['new_guids'] = len(results['guids'])
    results['cases'] = sorted(results['cases'].keys())
    results['cases_with_new_guids'] = len(results['cases'])
    if len(results['guids']):
        elasticsearchutils.clearCache()
    return results


def show_status(results):
    """
    Print status to stdout.

    :param results: the intermediate results to report.
    """
    sys.stdout.write('%d %d %d \r' % (
        results['processed'], len(results['guids']), len(results['cases'])))
    sys.stdout.flush()


def xml_to_object(obj):
    """
    Convert a general xml item to a python object.

    :param obj: an xml object as parsed by xmltodict
    :returns: a python object
    """
    data = obj
    if isinstance(obj, dict):
        data = {}
        if '#text' in obj:
            return obj['#text']
        for key in obj:
            value = xml_to_object(obj[key])
            if (key in ListNames and not isinstance(value, list) and
                    value is not None):
                value = [value]
            if value is not None:
                data[key] = value
        if not len(data):
            return None
    elif isinstance(obj, list):
        listed = []
        for value in obj:
            subval = xml_to_object(value)
            if subval is not None:
                listed.append(subval)
        if len(listed):
            return listed
    return data


if __name__ == '__main__':  # noqa
    db = 'parakon:9200'
    indexname = 'test1'
    listindexname = None
    clear = False
    for arg in sys.argv[1:]:
        if arg.startswith('--db='):
            db = arg.split('=', 1)[1]
        elif arg == '--delete':
            clear = True
        elif arg.startswith('--index='):
            indexname = arg.split('=', 1)[1]
        elif arg.startswith('--listindex='):
            listindexname = arg.split('=', 1)[1]
        elif arg.startswith('-'):
            print """Ingest refugee xml files into elasticsearch.

Syntax: ingest_entities.py (xml file ...) --db=(database url) --delete
                           --index=(index name) --listindex=(index name)

--db is the location of the database.  Default is parakon:9200.
--delete empties the index before loading data.
--index is the name of the index.  Default is test1.
--listindex is the name (possibly including wild cards) of the index that will
 list all known guids.  Known guids are not reimported.  Default is unset.
"""
            sys.exit(0)
    db = db.rstrip('/')
    if not db.startswith('http'):
        db = 'http://' + db
    indexname = indexname.strip('/')
    if listindexname:
        listindexname = '%s/%s' % (db, listindexname.strip('/'))
    if clear:
        delete_index(db, indexname)
        print '  Deleted'
    results = {}
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            continue
        print arg
        xmlfile = open(arg)
        ingest_xml(xmlfile, db, indexname, listindexname, results, show_status,
                   arg)
        sys.stdout.write('\n')
