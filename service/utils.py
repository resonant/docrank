import json
import os
import pymongo
import tangelo

# Used for caching the config values
CachedConfig = {}
CheckedLogIndices = False


def getDefaultConfig():
    """
    Fetch the defaults.json file.

    :returns: the parsed file.
    """
    return getNamedConfig('defaults.json')


def getNamedConfig(name):
    """
    Fetch a json config file, caching it if possible.

    :returns: the parsed file.
    """
    if name not in CachedConfig:
        scriptPath = os.path.dirname(os.path.realpath(__file__))
        filename = os.path.join(scriptPath, '..', name)
        if os.path.exists(filename):
            CachedConfig[name] = json.loads(open(filename).read())
        else:
            CachedConfig[name] = {}
    return CachedConfig[name]


def getLogDatabase():
    """
    Return a mongo db collection for the log database.

    :returns: a mongo collection object or None for error.
    """
    global CheckedLogIndices

    dburi = getDefaultConfig().get('stateLog')
    if not dburi:
        tangelo.http_status(501, 'Can\'t locate log db')
        return None
    conn = pymongo.MongoClient(dburi, connectTimeoutMS=60000)
    db = conn.get_default_database()
    coll = db['log']
    if not CheckedLogIndices:
        for index in [
                [('reference', pymongo.ASCENDING)],
                ]:
            coll.create_index(index)
        CheckedLogIndices = True
    return coll
