import elasticsearch
import json
import os
import tangelo

tangelo.paths(".")
import elasticsearchutils
import loadentityrecord
import utils


LocationData = None

NatoList = {
    'AA': u'Aruba',
    'AC': u'Antigua and Barbuda',
    'AE': u'United Arab Emirates',
    'AF': u'Afghanistan',
    'AG': u'Algeria',
    'AJ': u'Azerbaijan',
    'AL': u'Albania',
    'AM': u'Armenia',
    'AN': u'Andorra',
    'AO': u'Angola',
    'AQ': u'American Samoa',
    'AR': u'Argentina',
    'AS': u'Australia',
    'AU': u'Austria',
    'AV': u'Anguilla',
    'AY': u'Antarctica',
    'BA': u'Bahrain',
    'BB': u'Barbados',
    'BC': u'Botswana',
    'BD': u'Bermuda',
    'BE': u'Belgium',
    'BF': u'Bahamas',
    'BG': u'Bangladesh',
    'BH': u'Belize',
    'BK': u'Bosnia and Herzegovina',
    'BL': u'Bolivia',
    'BM': u'Myanmar',
    'BN': u'Benin',
    'BO': u'Belarus',
    'BP': u'Solomon Islands',
    'BR': u'Brazil',
    'BT': u'Bhutan',
    'BU': u'Bulgaria',
    'BV': u'Bouvet Island',
    'BX': u'Brunei',
    'BY': u'Burundi',
    'CA': u'Canada',
    'CB': u'Cambodia',
    'CD': u'Chad',
    'CE': u'Sri Lanka',
    'CF': u'Republic of the Congo',
    'CG': u'Democratic Republic of the Congo',
    'CH': u'China',
    'CI': u'Chile',
    'CJ': u'Cayman Islands',
    'CK': u'Cocos Islands',
    'CM': u'Cameroon',
    'CN': u'Comoros',
    'CO': u'Colombia',
    'CQ': u'Northern Mariana Islands',
    'CS': u'Costa Rica',
    'CT': u'Central African Republic',
    'CU': u'Cuba',
    'CV': u'Cape Verde',
    'CW': u'Cook Islands',
    'CY': u'Cyprus',
    'DA': u'Denmark',
    'DJ': u'Djibouti',
    'DO': u'Dominica',
    'DR': u'Dominican Republic',
    'EC': u'Ecuador',
    'EG': u'Egypt',
    'EI': u'Ireland',
    'EK': u'Equatorial Guinea',
    'EN': u'Estonia',
    'ER': u'Eritrea',
    'ES': u'El Salvador',
    'ET': u'Ethiopia',
    'EZ': u'Czech Republic',
    'FG': u'French Guiana',
    'FI': u'Finland',
    'FJ': u'Fiji',
    'FK': u'Falkland Islands',
    'FM': u'Micronesia',
    'FO': u'Faroe Islands',
    'FP': u'French Polynesia',
    'FR': u'France',
    'FS': u'French Southern Territories',
    'GA': u'Gambia',
    'GB': u'Gabon',
    'GG': u'Georgia',
    'GH': u'Ghana',
    'GI': u'Gibraltar',
    'GJ': u'Grenada',
    'GK': u'Guernsey',
    'GL': u'Greenland',
    'GM': u'Germany',
    'GP': u'Guadeloupe',
    'GQ': u'Guam',
    'GR': u'Greece',
    'GT': u'Guatemala',
    'GV': u'Guinea',
    'GY': u'Guyana',
    'HA': u'Haiti',
    'HK': u'Hong Kong',
    'HM': u'Heard Island and McDonald Islands',
    'HO': u'Honduras',
    'HR': u'Croatia',
    'HU': u'Hungary',
    'IC': u'Iceland',
    'ID': u'Indonesia',
    'IM': u'Isle of Man',
    'IN': u'India',
    'IO': u'British Indian Ocean Territory',
    'IR': u'Iran',
    'IS': u'Israel',
    'IT': u'Italy',
    'IV': u'Ivory Coast',
    'IZ': u'Iraq',
    'JA': u'Japan',
    'JE': u'Jersey',
    'JM': u'Jamaica',
    'JO': u'Jordan',
    'KE': u'Kenya',
    'KG': u'Kyrgyzstan',
    'KN': u'North Korea',
    'KR': u'Kiribati',
    'KS': u'South Korea',
    'KT': u'Christmas Island',
    'KU': u'Kuwait',
    'KV': u'Kosovo',
    'KZ': u'Kazakhstan',
    'LA': u'Laos',
    'LE': u'Lebanon',
    'LG': u'Latvia',
    'LH': u'Lithuania',
    'LI': u'Liberia',
    'LO': u'Slovakia',
    'LS': u'Liechtenstein',
    'LT': u'Lesotho',
    'LU': u'Luxembourg',
    'LY': u'Libya',
    'MA': u'Madagascar',
    'MB': u'Martinique',
    'MC': u'Macao',
    'MD': u'Moldova',
    'MF': u'Mayotte',
    'MG': u'Mongolia',
    'MH': u'Montserrat',
    'MI': u'Malawi',
    'MJ': u'Montenegro',
    'MK': u'Macedonia',
    'ML': u'Mali',
    'MN': u'Monaco',
    'MO': u'Morocco',
    'MP': u'Mauritius',
    'MR': u'Mauritania',
    'MT': u'Malta',
    'MU': u'Oman',
    'MV': u'Maldives',
    'MX': u'Mexico',
    'MY': u'Malaysia',
    'MZ': u'Mozambique',
    'NC': u'New Caledonia',
    'NE': u'Niue',
    'NF': u'Norfolk Island',
    'NG': u'Niger',
    'NH': u'Vanuatu',
    'NI': u'Nigeria',
    'NL': u'Netherlands',
    'NN': u'Sint Maarten',
    'NO': u'Norway',
    'NP': u'Nepal',
    'NR': u'Nauru',
    'NS': u'Suriname',
    'NT': u'Netherlands Antilles',
    'NU': u'Nicaragua',
    'NZ': u'New Zealand',
    'OD': u'South Sudan',
    'PA': u'Paraguay',
    'PC': u'Pitcairn',
    'PE': u'Peru',
    'PK': u'Pakistan',
    'PL': u'Poland',
    'PM': u'Panama',
    'PO': u'Portugal',
    'PP': u'Papua New Guinea',
    'PS': u'Palau',
    'PU': u'Guinea-Bissau',
    'QA': u'Qatar',
    'RE': u'Reunion',
    'RI': u'Serbia',
    'RM': u'Marshall Islands',
    'RN': u'Saint Martin',
    'RO': u'Romania',
    'RP': u'Philippines',
    'RQ': u'Puerto Rico',
    'RS': u'Russia',
    'RW': u'Rwanda',
    'SA': u'Saudi Arabia',
    'SB': u'Saint Pierre and Miquelon',
    'SC': u'Saint Kitts and Nevis',
    'SE': u'Seychelles',
    'SF': u'South Africa',
    'SG': u'Senegal',
    'SH': u'Saint Helena',
    'SI': u'Slovenia',
    'SL': u'Sierra Leone',
    'SM': u'San Marino',
    'SN': u'Singapore',
    'SO': u'Somalia',
    'SP': u'Spain',
    'ST': u'Saint Lucia',
    'SU': u'Sudan',
    'SV': u'Svalbard and Jan Mayen',
    'SW': u'Sweden',
    'SX': u'South Georgia and the South Sandwich Islands',
    'SY': u'Syria',
    'SZ': u'Switzerland',
    'TB': u'Saint Barthelemy',
    'TD': u'Trinidad and Tobago',
    'TH': u'Thailand',
    'TI': u'Tajikistan',
    'TK': u'Turks and Caicos Islands',
    'TL': u'Tokelau',
    'TN': u'Tonga',
    'TO': u'Togo',
    'TP': u'Sao Tome and Principe',
    'TS': u'Tunisia',
    'TT': u'East Timor',
    'TU': u'Turkey',
    'TV': u'Tuvalu',
    'TW': u'Taiwan',
    'TX': u'Turkmenistan',
    'TZ': u'Tanzania',
    'UC': u'Curacao',
    'UG': u'Uganda',
    'UK': u'United Kingdom',
    'UP': u'Ukraine',
    'US': u'United States',
    'UV': u'Burkina Faso',
    'UY': u'Uruguay',
    'UZ': u'Uzbekistan',
    'VC': u'Saint Vincent and the Grenadines',
    'VE': u'Venezuela',
    'VI': u'British Virgin Islands',
    'VM': u'Vietnam',
    'VQ': u'U.S. Virgin Islands',
    'VT': u'Vatican',
    'WA': u'Namibia',
    'WE': u'Palestinian Territory',
    'WF': u'Wallis and Futuna',
    'WI': u'Western Sahara',
    'WS': u'Samoa',
    'WZ': u'Swaziland',
    'YI': u'Serbia and Montenegro',
    'YM': u'Yemen',
    'ZA': u'Zambia',
    'ZI': u'Zimbabwe',
}


def getEntityPlacesFromFile(guid, entityId):
    """
    Use an external file to get pre-computed locations based on a twitter
    handle.

    :param guid: guid of the main user.
    :param entityId: twitter handle.
    :returns: a set of locations.
    """
    global LocationData

    entityId = entityId.split(':')[-1].lower()
    if not LocationData:
        scriptPath = os.path.dirname(os.path.realpath(__file__))
        filename = os.path.join(scriptPath, '..', 'locations.dat')
        if os.path.exists(filename):
            LocationData = {}
            for line in open(filename):
                parts = line.decode('utf8').strip().split('\t')
                if len(parts) < 2:
                    continue
                key = (parts[0].lower(), )
                LocationData[key] = set(parts[1:])
            tangelo.log('Loaded location information for %d entities' % (
                len(LocationData)))
    key = (entityId, )
    if not LocationData or key not in LocationData:
        return set()
    return LocationData[key]


def getGuidPlaces(guid):
    """
    Parse an entity for places.

    :param guid: guid to lookup and parse.
    :returns: a set of locations.
    """
    dbkey = 'getGuidPlaces'
    if elasticsearchutils.cacheResults(dbkey, guid):
        return elasticsearchutils.cacheResults(dbkey, guid)
    entity = json.loads(loadentityrecord.run(guid))['result']
    places = set()
    for identity in entity.get('Identity', []):
        for key in ('Citizenship', 'Country Of Birth', 'FullPlaceOfBirth'):
            if key in identity:
                places |= set([identity[key].title()])
        for addr in identity.get('Address', []):
            if 'City' in addr:
                places |= set([addr['City'].title()])
            if 'Country' in addr and addr['Country'] in NatoList:
                places |= set([NatoList[addr['Country']]])
    elasticsearchutils.cacheResults(dbkey, guid, places, 1)
    return places


def run(guid, entityId, *args, **kwargs):
    # Create an empty response object.
    response = {}

    entities = elasticsearchutils.getEntitiesForGuid(guid)
    entity = None
    for testEntity in entities:
        if testEntity.get('id') == entityId:
            entity = testEntity
            break
    if entity and entity.get('query') is not None:
        entities = elasticsearchutils.getEntitiesForGuid(guid, entity['query'])
        for testEntity in entities:
            if testEntity.get('id') == entityId:
                entity = testEntity
                break
    response['result'] = entity

    if entity.get('type') == 'twitter_user':
        # Get location comparison information
        collection = utils.getDefaultConfig()[
            elasticsearchutils.NameRankingsDbKey]
        es = elasticsearch.Elasticsearch(collection, timeout=300)
        query = {
            'query': {'function_score': {'query': {'bool': {'must': [
                {'match': {'PersonGUID': guid}},
            ]}}}},
        }
        res = es.search(body=json.dumps(query))
        guidPlaces = set()
        entityPlaces = set()
        entityName = entity['name'].lower()
        for hit in res['hits']['hits']:
            record = hit['_source']
            for cat in record.get('known_locations', {}):
                guidPlaces |= set([place.strip().title() for place in
                                   record['known_locations'][cat]])
            for match in record.get('matches', []):
                if (entityName == match.get('name', '').lower() or
                        entityName == match.get('search_term', '').lower()):
                    entityPlaces |= set([place.strip().title() for place in
                                         match.get('match_locations', [])])
        guidPlaces |= getGuidPlaces(guid)
        if len(guidPlaces):
            response['places'] = sorted(list(guidPlaces))
            if not len(entityPlaces):
                entityPlaces = getEntityPlacesFromFile(guid, entityId)
        if len(entityPlaces):
            response['entity_places'] = sorted(list(entityPlaces))

    # Return the response object.
    # tangelo.log(str(response))
    return json.dumps(response)
