import cherrypy
import sys
import tangelo
import tangelo.__main__


cherrypy.config.update({
    'server.thread_pool': 1000
})
sys.argv[0] = 'tangelo'
tangelo.__main__.main()
