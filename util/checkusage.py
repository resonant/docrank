import json
import os
import time

url = ('http://10.1.93.230/resdoc/service/getlogs?pretty=true&limit=%d&'
       'skip=%d&projection={%%22state.session%%22:true,'
       '%%22state.numActions%%22:true,%%22state.lastActionTime%%22:true,'
       '%%22state.lineupCol%%22:true,'
       '%%22state.guidData%%22:true,'
       '%%22_id%%22:false}')
chunk = 50
sessions = {}


def count_guid_actions(data):
    numGuid = len(data)
    numMatches = [0, 0, 0, 0]   # total, derog==false, derog==true, checked
    numDocs = [0, 0, 0]
    for guid in data:
        for match in data[guid].get('matches', {}):
            numMatches[0] += 1
            if data[guid]['matches'][match].get('derog', None) == False:
                numMatches[1] += 1
            if data[guid]['matches'][match].get('derog', None) == True:
                numMatches[2] += 1
            if data[guid]['matches'][match].get('check', None) == True:
                numMatches[3] += 1
        for doc in data[guid].get('documents', {}):
            numDocs[0] += 1
            if data[guid]['documents'][doc].get('derog', None) == False:
                numDocs[1] += 1
            if data[guid]['documents'][doc].get('derog', None) == True:
                numDocs[2] += 1
    return {'guids': numGuid, 'matches': numMatches, 'docs': numDocs}


print '%-16s %9s %-19s %5s' % ('Session name', '# actions', 'Last use',
                               'Days ago')
for offset in xrange(0, 100000, chunk):
    dataurl = url % (chunk, offset)
    cmd = "curl -s -g '%s'" % dataurl.replace("'", "'\\''")
    data = json.loads(os.popen(cmd).read())
    if not len(data['result']) or offset >= data['count']:
        break
    for row in data['result']:
        if 'state' not in row:
            continue
        session = row['state']['session']
        date = row['state']['lastActionTime'] * 0.001
        count = row['state']['numActions']
        if count > sessions.get(session, 0):
            stats = count_guid_actions(row['state'].get('guidData', {}))
            print '%-16s %9d %19s %8.2f  %d  %d %d %d %d  %d %d %d' % (
                session, count,
                time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(date)),
                (time.time()-date)/86400,
                stats['guids'],
                stats['matches'][0], stats['matches'][3],
                stats['matches'][1], stats['matches'][2],
                stats['docs'][0], stats['docs'][1], stats['docs'][2],
                )
        sessions[session] = count
