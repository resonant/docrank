/* jslint browser:true */
/* global docrankApp, actionState */
'use strict';

/* Perform an ajax call, tracking it so it can be cleared if desired.  A
 * variety of actions can be taken:
 *  replace: aborts all extant (category) calls, then makes a new call.  If
 *      (action) is null, this is used.
 *  cancel: aborts all extant (category) calls
 *  add: makes a new call without affecting old calls.
 *
 * @param category: a key for this call.  All items with the same key can be
 *                  cancelled at once.
 * @param action: see above.
 * @param params: the jquery parameters for the ajax call.
 */
docrankApp.ajax = function (category, action, params) {
    if ($.isArray(category)) {
        _.each(category, function (cat) {
            docrankApp.ajax(cat, action, params);
        });
        return;
    }
    if (!docrankApp.ajaxCalls) {
        docrankApp.ajaxCalls = {};
        docrankApp.ajaxNumber = 0;
    }
    if (action === null || action === 'replace' || action === 'cancel') {
        _.each(docrankApp.ajaxCalls, function (xhr, idx) {
            if (xhr.category === category || idx === category) {
                xhr.abort();
                delete docrankApp.ajaxCalls[idx];
            }
        });
        if (action === 'cancel') {
            return;
        }
    }
    var oldcomp = params.complete;
    var num = docrankApp.ajaxNumber;
    params = $.extend({}, params);
    params.complete = function () {
        delete docrankApp.ajaxCalls[num];
        if (oldcomp) {
            oldcomp.apply(this, arguments);
        }
    };
    var xhr = $.ajax(params);
    xhr.category = category;
    xhr.callNumber = num;
    docrankApp.ajaxCalls[num] = xhr;
    docrankApp.ajaxNumber += 1;
};

/* This should be called whenever the actionState is changed.  This will log
 * the results and update routing.
 *
 * @param action: the action that caused the change.
 * @param allowMerge: if true and the action is the same as the last, don't add
 *                    a new navigation point.
 * @param immediate: log as soon as possible, rather than at a limited rate.
 */
docrankApp.logState = function (action, allowMerge, immediate) {
    if (!docrankApp.logStateInfo) {
        docrankApp.logStateInfo = {
            last: {}, secondlast: {}, delta: [], merge: null
        };
    }
    var info = docrankApp.logStateInfo;
    var merge = (action === actionState.lastAction && allowMerge);
    var delta;
    actionState.lastAction = action;
    actionState.lastActionTime = new Date().getTime();
    if (!merge) {
        actionState.numActions = (actionState.numActions || 0) + 1;
        delta = docrankApp.objDiff(info.last, actionState);
        info.delta.push(delta);
        info.secondlast = info.last;
    } else {
        delta = docrankApp.objDiff(info.secondlast, actionState);
        if (info.delta.length) {
            info.delta[info.delta.length - 1] = delta;
        } else {
            info.delta.push(delta);
        }
    }
    info.last = $.extend(true, {}, actionState);
    info.merge = (info.merge === null ? merge : (info.merge && merge));
    /* Routing goes here, including handling merges */
    if (info.timer && immediate) {
        window.clearTimeout(info.timer);
        info.timer = null;
    }
    info.needed = true;
    if (!info.timer && !info.sending) {
        info.timer = window.setTimeout(docrankApp.logStateSend, 1);
    }
};

/* Send the action state to the log script.  If there is no state to send, exit
 * without doing anything.  Otherwise, after sending the state, wait some time
 * and check if it needs to be sent again.
 */
docrankApp.logStateSend = function () {
    var info = docrankApp.logStateInfo;
    if (info.sending) {
        return;
    }
    if (info.timer) {
        window.clearTimeout(info.timer);
        info.timer = null;
    }
    if (!info.needed) {
        return;
    }
    info.needed = false;
    info.sending = true;
    var merge = info.merge;
    info.merge = null;
    $.ajax({
        url: 'service/logstate',
        data: JSON.stringify({'state': actionState, 'delta': info.delta}),
        method: 'POST',
        processData: false,
        contentType: 'application/json',
        success: function (response) {
            docrankApp.router.navigate(
                response.reference, {replace: merge === true});
        },
        complete: function () {
            info.sending = false;
            info.timer = window.setTimeout(docrankApp.logStateSend, 5000);
        }
    });
    info.delta = [];
};

/* Return only the fields that have changed between two objects.
 *
 * @param a: the basis object
 * @param b: the new object.
 * @returns: the delta object.
 */
docrankApp.objDiff = function (a, b) {
    var delta = {};
    _.each(b, function (value, key) {
        if (!_.isEqual(a[key], value)) {
            if (_.isObject(a[key]) && !_.isArray(a[key]) && _.isObject(value) && !_.isArray(value)) {
                delta[key] = docrankApp.objDiff(a[key], value);
            } else {
                delta[key] = value;
            }
        }
    });
    _.each(a, function (value, key) {
        if (b[key] === undefined) {
            delta[key] = undefined;
        }
    });
    return delta;
};

/* Parse a query string.
 *
 * @param querystring: string to parse.  location.search is a common.
 * @returns: a dictionary of query parameters.
 */
docrankApp.parseQueryString = function (queryString) {
    var params = {};
    if (queryString) {
        if (queryString.indexOf('?') === 0) {
            queryString = queryString.substr(1);
        }
        _.each(queryString.replace(/\+/g, ' ').split(/&/g), function (el) {
            var aux = el.split('='), val;
            if (aux.length > 1) {
                val = decodeURIComponent(el.substr(aux[0].length + 1));
            }
            params[decodeURIComponent(aux[0])] = val;
        });
    }
    return params;
};

docrankApp.router = new (Backbone.Router.extend({
    routes: {
        ':stateref': 'stateref'
    }
}))();

/* Add a function to jquery to calculate the width of a scroll bar.
 *
 * @returns: the width of the scroll bar.
 */
$.scrollbarWidth = function () {
    var parent, child, width;

    if (width === undefined) {
        parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
        child = parent.children();
        width = child.innerWidth() - child.height(99).innerWidth();
        parent.remove();
    }
    return width;
};
