/* jslint browser:true, unparam:true */
/* globals $, console, d3, log, logSetupLineUp, initializeLoggingFramework, logSelectLineUpEntry, LineUp, sprintf, Mousetrap */
'use strict';

var docrankApp = {};

var lineUpConfig = {
    interaction: {
        tooltips: false
    },
    renderingOptions: {
        animation: true
    },
    svgLayout: {
        mode: 'separate',
        rowPadding: 0
    }
};
var lineup = {};

/* The action state, when saved, should allow returning to the exact same
 * interface state if the data in the databases is the same. */
var actionState = {};
/* This is data associated with the current action state but not necessary to
 * return to that state. */
var actionData = {};


/* Create or recreate a lineup control.
 *
 * @param elem: selector to the parent div wrapper for the control.
 * @param name: name of the control.
 * @param desc: column description.
 * @param dataset: dataset to load.
 * @lineupObj: old lineup object to replace.
 * @sort: if specified, sort by this column.
 * @returns: a lineup control object.
 */
function createLineup(elem, name, desc, dataset, lineupObj, sort) {
    /* This has been parroted from th demo. */
    var spec = {};
    spec.name = name;
    spec.dataspec = desc;
    delete spec.dataspec.file;
    delete spec.dataspec.separator;
    spec.dataspec.data = dataset;
    spec.storage = LineUp.createLocalStorage(
        dataset, desc.columns, desc.layout, desc.primaryKey);
    var config = ((lineupObj ? lineupObj.config :
                   $.extend({}, lineUpConfig)) || {});
    if (!config.renderingOptions) {
        config.renderingOptions = {};
    }
    var oldAnimation = config.renderingOptions.animation;
    config.renderingOptions.animation = false;
    var columnFixed = 5;
    var scale = createLineupAdjustWidth(elem, name, spec, columnFixed);
    /* Never take the first branch if we always want to regenerate the lineup
     * container.  There is a bug where the columns are not clipped properly,
     * and regeneration works around it.  Both cases bleed memory, I think, and
     * regeneration is worse than update. */
    if (lineupObj && 0) {
        lineupObj.changeDataStorage(spec);
    } else {
        $(elem).empty();
        /* Lineup takes a d3 element */
        lineupObj = LineUp.create(spec, d3.select(elem), lineUpConfig);
        config = lineupObj.config;
        lineupObj.dragWeight.on('dragend.docrank', function (evt) {
            lineupDragColumnEnd(name, evt);
        });
    }
    lineupObj['column-scale'] = scale;
    lineupObj['column-fixed'] = columnFixed;
    lineupObj['lineup-key'] = name;
    $(elem).attr('lineup-key', name);
    if (sort) {
        lineupObj.sortBy(sort);
    }
    config.renderingOptions.animation = oldAnimation;
    var fixTooltips = function () {
        for (var i = 0; i < desc.columns.length; i += 1) {
            if (desc.columns[i].description) {
                var label = (desc.columns[i].label || desc.columns[i].column);
                $('title', $(elem + ' .lu-header text.headerLabel:contains("' + label + '")').parent()).text(
                    label + ': ' + desc.columns[i].description);
            }
        }
    };
    /* Try twice to work around some issues */
    window.setTimeout(fixTooltips, 1);
    window.setTimeout(fixTooltips, 1000);
    return lineupObj;
}

/* Adjust the width of the columns in lineup to (a) use the available space,
 * and (b) use the weights selected by the user.
 *
 * @param elem: the element where the lineup is placed.
 * @param name: the name used for this lineup.  Used for tracking user widths.
 * @param spec: the specification for the lineup.  Modified.
 * @param fixed: fixed width used in each column.
 * @returns: relative scale of lineup to available space.
 */
function createLineupAdjustWidth(elem, name, spec, fixed) {
    var total = 0, count = 0, c1, c2;
    var width = $(elem)[0].getBoundingClientRect().width - $.scrollbarWidth() - fixed;
    var col = spec.dataspec.layout.primary;
    for (c1 = 0; c1 < col.length; c1 += 1) {
        if (col[c1].children) {
            for (c2 = 0; c2 < col[c1].children.length; c2 += 1) {
                count += 1;
                total += lineupGetColumnWidth(name, col[c1].children[c2], fixed);
            }
        } else {
            count += 1;
            total += lineupGetColumnWidth(name, col[c1], fixed);
        }
    }
    var avail = width - count * fixed;
    var scale = avail / total;
    for (c1 = 0; c1 < col.length; c1 += 1) {
        if (col[c1].children) {
            for (c2 = 0; c2 < col[c1].children.length; c2 += 1) {
                col[c1].children[c2].width = fixed + col[c1].children[c2].widthBasis * scale;
            }
        } else {
            col[c1].width = fixed + col[c1].widthBasis * scale;
        }
    }
    return scale;
}

/* Examine the currently selected person.  This asks to load information about
 * the person and to show potentially matching entities.
 */
function examinePerson() {
    var guid = $('#person-list select').val();
    if (!guid || $('.modal').is(':visible')) {
        return;
    }
    $('#examine-button').focus();
    if (guid.indexOf(' - ') >= 0) {
        guid = guid.substr(0, guid.indexOf(' - '));
    }
    actionState.guid = guid;
    docrankApp.logState('examine');
    interfaceShowHide('match-controls', ['document-controls', 'document-panel', 'docuser-panel', 'match-panel']);
    initializeLineUpAroundEntity(guid);
    getEntityJSON(guid);
}

/* Based on the current case, get information for all persons, all potential
 * matches, and all examined documents, and present it in a report.
 */
function generateReport() {
    if (!$('#doc-report-button').is(':visible') || $('.modal').is(':visible')) {
        return;
    }
    if (!actionState.case) {
        alert('No case selected.  Cannot generate a case report.');
        return;
    }
    /* Load a list of persons associated with the current case */
    var report = {
        case: actionState.case,
        requests: 0,
        totalRequests: 0,
        entities: {}
    };
    docrankApp.logState('generateReport');
    generateReportStep(report);
    docrankApp.ajax('report', 'replace', {
        url: 'service/getcasenames/' + actionState.case,
        dataType: 'json',
        success: function (response) {
            var res = response.result;
            report.order = res.order;
            _.each(res.order, function (guid) {
                /* Load the personal information for each entity associated
                 * with the case */
                generateReportStep(report);
                report.entities[guid] = {
                    description: res.guids[guid],
                    matches: {}
                };
                docrankApp.ajax('report', 'add', {
                    url: 'service/loadentityrecord/' + encodeURIComponent(guid),
                    dataType: 'json',
                    success: function (response) {
                        report.entities[guid].record = response.result;
                        generateReportShow(report);
                    }
                });
                /* Load the potential entity matches for each entity associated
                 * with the case */
                generateReportStep(report);
                docrankApp.ajax('report', 'add', {
                    url: 'service/lineupuser/' + encodeURIComponent(guid),
                    dataType: 'json',
                    success: function (response) {
                        generateReportMatch(report, guid, response);
                        generateReportShow(report);
                    }
                });
            });
            generateReportShow(report);
        }
    });
}

/* For each matching entity, check which were examined and populate document
 * information in the report.
 *
 * @param report: the report object to build up.
 * @param guid: the guid of the person associated with this data.
 * @param response: the list of potential matches associated with an entity.
 */
function generateReportMatch(report, guid, response) {
    var dataset = response.result;
    _.each(dataset, function (match) {
        var matches = report.entities[guid].matches;
        matches[match.id] = {
            state: actionState.matches[match.id],
            data: match,
            documents: {}
        };
        if (actionState.matches[match.id] && actionState.matches[match.id].check) {
            /* Load the associated documents for each potential match that was
             * examined. */
            generateReportStep(report);
            docrankApp.ajax('report', 'add', {
                url: 'service/lineupuserdoc/' + encodeURIComponent(guid) + '/' + encodeURIComponent(match.id),
                dataType: 'json',
                success: function (response) {
                    _.each(response.result, function (result) {
                        _.each(result.result, function (doc) {
                            /* Get each document */
                            var docstate = actionState.documents[doc.id];
                            matches[match.id].documents[doc.id] = {
                                state: docstate
                            };
                            if (docstate && docstate.derog !== undefined && docstate.derog !== null) {
                                generateReportStep(report);
                                docrankApp.ajax('report', 'add', {
                                    url: 'service/getdocument/' + encodeURIComponent(guid) + '/' + encodeURIComponent(match.id) + '/' + encodeURIComponent(doc.id),
                                    dataType: 'json',
                                    success: function (response) {
                                        matches[match.id].documents[doc.id].doc = response.result || doc;
                                        generateReportShow(report);
                                    }
                                });
                            }
                        });
                    });
                    generateReportShow(report);
                }
            });
        }
    });
}

/* Update the generate report progress bar.
 *
 * @param report: the current state of the report.
 */
function generateReportProgress(report) {
    var percent = 100.0 * (
        report.totalRequests - report.requests) / report.totalRequests;
    var progressText = sprintf(
        '%d / %d (%3.1f%%)', report.totalRequests - report.requests,
        report.totalRequests, percent);
    $('#report-panel .progress .progress-bar').attr({
        'aria-valuemax': report.totalRequests,
        'aria-valuenow': report.requests
    }).css('width', percent + '%');
    $('#report-panel .progress .progress-text').text(progressText);
}

/* Check if all requests necessary for building a report have finished.  If so,
 * show the report.
 *
 * @param report: the generated or in-progress report.
 */
function generateReportShow(report) {
    report.requests -= 1;
    generateReportProgress(report);
    if (report.requests) {
        return;
    }
    $('#report-results').empty();
    docrankApp.logState('generateReportShow');
    /* This is a very poor way to generate a report, but all possible data is
     * present. */
    console.log(report);
    $('#report-results').html(docrankApp.templates.report({data: report}));
    $('#report-panel .progress').hide();
    $('#report-results').show();
    $('#report-results').off('hidden.bs.modal')
        .on('hidden.bs.modal', function () {
            docrankApp.ajax('report', 'cancel');
        });
}

/* Mark that we need to fetch more data for generating a report.
 *
 * @param report: the in-progress report.
 */
function generateReportStep(report) {
    report.requests += 1;
    report.totalRequests += 1;
    generateReportProgress(report);
    if (report.totalRequests === 1) {
        $('#report-panel .progress').show();
        $('#report-results').empty().hide();
        $('#report-panel .modal-header h3').text('Report for ' + report.case);
        $('#report-panel').modal();
    }
}

function getEntityJSON(handle) {
    docrankApp.ajax('loadentityrecord', 'replace', {
        url: 'service/loadentityrecord/' + encodeURIComponent(handle),
        dataType: 'json',
        success: function (res) {
            $('#graph1-json-info').empty();
            docrankApp.jsonToTable('#graph1-json-info', res.result);
            infoTableHeight('#graph1-json-info');
        }
    });
}

/* Adjust the height of an info table to take up available space.
 *
 * @param elem: the element with the table.
 */
function infoTableHeight(elem) {
    elem = $(elem);
    var h = elem.closest('.container-fluid').innerHeight();
    h -= elem.position().top;
    h -= 5;
    elem.height(h + 'px');
}

function initializeLineUpAroundEntity(handle) {
    logSetupLineUp();

    interfaceShowHide(null, 'match-panel');
    docrankApp.ajax('lineupuser', 'replace', {
        url: 'service/lineupuser/' + encodeURIComponent(handle),
        dataType: 'json',
        success: function (response) {
            var dataset = response.result, desc = response, i;
            actionData.lineup = response;
            if (!actionState.guidData) {
                actionState.guidData = {};
            }
            if (!actionState.guidData[actionState.guid]) {
                actionState.guidData[actionState.guid] = {
                    matches: {},
                    documents: {}
                };
            }
            actionState.matches = actionState.guidData[actionState.guid].matches;
            docrankApp.logState('currentMatches');
            /* populate enabled and confidence on the dataset */
            for (i = 0; i < dataset.length; i += 1) {
                var row = dataset[i];
                if (actionState.matches[row.id]) {
                    updateMatchOptions(
                        null, row, actionState.matches[row.id], false);
                }
            }
            lineup.main = createLineup(
                '#lugui-wrapper', 'main', desc, dataset, lineup.main, 'Combined');
            lineup.main.on('selected', selectUserFromLineup);
            for (i = 0; i < desc.columns.length; i += 1) {
                if (desc.columns[i].description) {
                    $('title', $('#lugui-wrapper .lu-header .header text:contains("' + desc.columns[i].label + '")').parent()).text(desc.columns[i].label + ': ' + desc.columns[i].description);
                }
            }
            interfaceShowHide('match-controls');
        }
    });
}

/* Show or hide parts of the interface.  If hiding them, abort any queries that
 * are for that section.
 *
 * @param show: an element or a list of elements to show.  This may be null or
 *              undefined.
 * @param hide: an element or a list of elements to hide.  This may be null or
 *              undefined.
 */
function interfaceShowHide(show, hide) {
    if (hide) {
        if (!$.isArray(hide)) {
            hide = [hide];
        }
        _.each(hide, function (elem) {
            $('#' + elem).css('display', 'none');
            switch (elem) {
                case 'document-controls':
                    docrankApp.ajax('lineupuserdoc', 'cancel');
                    break;
                case 'document-panel':
                    docrankApp.ajax(['getdocument'], 'cancel');
                    break;
                case 'match-controls':
                    docrankApp.ajax(['loadentityrecord', 'lineupuser'], 'cancel');
                    break;
                case 'match-panel':
                    docrankApp.ajax('getpotentialuser', 'cancel');
                    break;
            }
        });
    }
    if (show) {
        if (!$.isArray(show)) {
            show = [show];
        }
        _.each(show, function (elem) {
            $('#' + elem).css('display', 'block');
        });
    }
}

/* After a column is resized in lineup, record the size it became relative to
 * the scaling we are using.
 *
 * @param name: name of the lineup record we have adjusted.
 */
function lineupDragColumnEnd(name) {
    var c1, c2;
    if (!actionState.lineupCol) {
        actionState.lineupCol = {};
    }
    if (!actionState.lineupCol[name]) {
        actionState.lineupCol[name] = {};
    }
    var record = actionState.lineupCol[name];
    var col = lineup[name].storage.getColumnLayout();
    var scale = lineup[name]['column-scale'];
    var fixed = lineup[name]['column-fixed'];
    for (c1 = 0; c1 < col.length; c1 += 1) {
        if (col[c1].children) {
            for (c2 = 0; c2 < col[c1].children.length; c2 += 1) {
                record[col[c1].children[c2].columnLink] = (col[c1].children[c2].columnWidth - fixed) / scale;
            }
        } else {
            record[col[c1].columnLink || 'rank'] = (col[c1].columnWidth - fixed) / scale;
        }
    }
    docrankApp.logState('lineupColumns');
}

/* Get the width of a column.  If the user has changed the width, scale based
 * on that activity.
 *
 * @param name: name of the lineup.  Used for user settings.
 * @param col: column specification.
 * @param fixed: minimum width for a column.
 */
function lineupGetColumnWidth(name, col, fixed) {
    var width = col.width || 0;
    var colname = col.column || col.type;
    /* Get the column scaling based on the user settings, if available */
    if (actionState.lineupCol && actionState.lineupCol[name] && actionState.lineupCol[name][colname]) {
        width = actionState.lineupCol[name][colname] + fixed;
    }
    var colWidth = width < fixed ? 0 : (width - fixed);
    col.widthBasis = colWidth;
    col.widthFixed = fixed;
    return col.widthBasis;
}

// do a non-blocking call to a python service that returns all the names in the graph.  Assign this to a global variable
function loadCaseList() {
    logSystemActivity('graph_A_group', '#graph1', 'inspect', 'OPEN');

    docrankApp.ajax('loadcaselist', 'replace', {
        url: 'service/getcases',
        dataType: 'json',
        success: function (response) {
            actionData.caselist = response.result.allnodes;
            // console.log('case list:', actionData.caselist);
            // Enable name selection and update the list
            $('#ga-name').prop('disabled', false);
            updateUserList(response.result.nodes, response.result.allnodes);
            $('#ga-nodeCount').text(response.result.allnodes.length);
            $('#ga-nodeCountB').text(response.result.nodes.length);
            Backbone.history.start();
        }
    });
}

// logging is handled largely in
function logSystemActivity(group, element, activityEnum, action, tags) {
    group = typeof group !== 'undefined' ? group : 'system_group';
    activityEnum = typeof activityEnum !== 'undefined' ? activityEnum : 'show';
    action = typeof action !== 'undefined' ? action : 'SHOW';
    tags = typeof tags !== 'undefined' ? tags : [];
    var msg = {
        activity: activityEnum,
        action: action,
        elementId:  element,
        elementType: 'OTHER',
        elementGroup: group,
        source: 'system',
        tags: tags
    };
    if (typeof log === 'function') {
        log(msg);
    }
}

/* Select the next case in the list and examine the first person in that case.
 */
function nextCase() {
    var pos = $.inArray($('#ga-name').val(), actionData.caselist) + 1;
    if (pos < 0 || pos >= actionData.caselist.length) {
        pos = 0;
    }
    $('#ga-name').val(actionData.caselist[pos]);
    updatePersonList(null, true);
}

/* Select the next person in the case which has documents.  If on the last such
 * person, go to the next case.
 */
function nextPerson() {
    var pos = $('#person-list select')[0].selectedIndex;
    var opt = $('#person-list select option').eq(pos + 1);
    if (opt.length && !opt.is('.no-documents')) {
        opt.prop('selected', true);
        examinePerson();
    } else {
        nextCase();
    }
}

/* Set or get the value of a set of paired checkboxes.
 *
 * @param: id: the base id of the pair.  One must be named (id)-n and one
 *             (id)-y.
 * @param action: if 'set', set the checkboxes with the specified value.
 *                Otherwise, get the current value.
 * @param value: the value to set.  false or 'no' checks the (id)-n box, true
 *               or 'yes' checks the (id)-y box.
 * @returns: the current value.  One of undefined, false, or true.
 */
function pairedCheckbox(id, action, value) {
    if (action === 'set') {
        if (value === 'yes') {
            value = true;
        } else if (value === 'no') {
            value = false;
        } else if (value !== false && value !== true) {
            value = undefined;
        }
        $('#' + id + '-n').prop('checked', value === false);
        $('#' + id + '-y').prop('checked', value === true);
    } else {
        value = undefined;
        if ($('#' + id + '-n').is(':checked')) {
            value = false;
        } else if ($('#' + id + '-y').is(':checked')) {
            value = true;
        }
    }
    return value;
}

/* Handle a change event for a pair of checkboxes.  Don't allow both to be
 * checked.
 *
 * @param evt: the event that triggered the change.
 */
function pairedCheckboxChanged(evt) {
    var elem = $(evt.target);
    var pairId = elem.attr('id');
    var otherId = pairId.substr(0, pairId.length - 1) + (
        pairId.substr(pairId.length - 1) === 'n' ? 'y' : 'n');
    var otherElem = $('#' + otherId);
    if (elem.is(':checked') && otherElem.is(':checked')) {
        otherElem.prop('checked', false);
        otherElem.trigger('change');
    }
}

/* Return a user string for the state of a paired check box.
 *
 * @param value: the value of the checkbox.
 * @returns: a text string associated with the value.
 */
function pairedCheckboxText(value) {
    if (value === false || value === 'no') {
        return 'no';
    }
    if (value === true || value === 'yes') {
        return 'yes';
    }
    return '';
}

/* When we are told to navigate to a particular state, we need to retreive it
 * and then switch what we are doing.
 *
 * @param statehash: the value in the routing string.
 */
function restoreState(statehash) {
    docrankApp.ajax('getstate', 'replace', {
        url: 'service/getstate/' + encodeURIComponent(statehash),
        dataType: 'json',
        success: function (response) {
            var state = response.result;
            if (!state) {
                return;
            }
            if (!state.oldsession) {
                state.oldsession = [];
            }
            state.oldsession.push({
                numActions: state.numActions,
                session: state.session,
                reference: statehash
            });
            state.session = actionState.session;
            /* We want to load the interface to where it had been, showing the
             * selected case, person, potential match, and document.  It would
             * also be nice to pull overrides from the query string. */
            console.log(state);
            actionState = state;
        }
    });
}

/* Given a selection of possible matches, show the documents for them.
 */
function reviewDocuments() {
    if (!$('#review-button').is(':visible') || $('.modal').is(':visible')) {
        return;
    }
    var docs = [];
    for (var i = 0; i < actionData.lineup.result.length; i += 1) {
        var row = actionData.lineup.result[i];
        if (actionState.matches[row.id] && actionState.matches[row.id].check) {
            docs.push({
                row: row, match: actionState.matches[row.id], id: row.id});
        }
    }
    if (!docs.length) {
        alert('No possible matches were indicated, therefore there are no documents to review.');
        return;
    }
    actionData.matchedRows = docs;
    actionState.currentRow = 0;
    actionState.documents = actionState.guidData[actionState.guid].documents;
    docrankApp.logState('reviewDocuments');
    interfaceShowHide('document-controls', ['document-panel', 'docuser-panel', 'match-controls', 'match-panel']);
    $('#doc-prev-button').toggleClass('disabled', docs.length <= 1);
    $('#doc-next-button').toggleClass('disabled', docs.length <= 1);
    updateDocumentLineup();
}

/* Review the next possible match of documents in the list.
 */
function reviewNext() {
    if (!$('#doc-next-button').is(':visible') || $('.modal').is(':visible') || $('#doc-next-button').is('.disabled')) {
        return;
    }
    var len = actionData.matchedRows.length;
    actionState.currentRow = (actionState.currentRow + 1) % len;
    docrankApp.logState('reviewNext');
    updateDocumentLineup();
}

/* Review the previous possible match of documents in the list.
 */
function reviewPrevious() {
    if (!$('#doc-prev-button').is(':visible') || $('.modal').is(':visible') || $('#doc-next-button').is('.disabled')) {
        return;
    }
    var len = actionData.matchedRows.length;
    actionState.currentRow = (actionState.currentRow + len - 1) % len;
    docrankApp.logState('reviewPrevious');
    updateDocumentLineup();
}

/* Select the document match (the user associated with documents), showing
 * appropriate information.
 */
function selectDocMatch() {
    $('#doc-match-rankings').addClass('selected');
    interfaceShowHide('docuser-panel', 'document-panel');
    _.each(lineup, function (lineupObj, lineupkey) {
        lineup[lineupkey].select(null);
    });
}

/* Select a row in lineup.
 *
 * @param row: the row of data we passed to lineup.
 */
function selectDocumentFromLineup(row) {
    logSelectLineUpEntry();

    if (!row || !row.id) {
        if (!$('.lugui-doc-container .selected').length) {
            interfaceShowHide(null, 'document-panel');
        }
        return;
    }
    $('#doc-match-rankings').removeClass('selected');
    var key = row.source || 'original';
    _.each(lineup, function (lineupObj, lineupkey) {
        if (key !== lineupkey) {
            lineup[lineupkey].select(null);
        }
    });
    selectDocumentFromLineupProcess(null, row);
    var entityRow = actionData.matchedRows[actionState.currentRow];
    docrankApp.ajax('getdocument', 'replace', {
        url: 'service/getdocument/' + encodeURIComponent(actionState.guid) + '/' + encodeURIComponent(entityRow.id) + '/' + encodeURIComponent(row.id),
        dataType: 'json',
        success: selectDocumentFromLineupProcess
    });
}

/* Update the right panel with information about the selected document.  This
 * is expected to be called twice: once upon first selection with data we
 * already have, and once when additional data is returned from an ajax call.
 *
 * @param response: response from the ajax call or null to indicate this is
 *                  called with row data.
 * @param row: if response is null, the known row data.
 */
function selectDocumentFromLineupProcess(response, row) {
    var doc = row;
    if (response !== null) {
        row = actionData.lineupdocrow;
        doc = response.result || row;
    }
    var id = row.id;
    var link;
    if (doc.doc_type === 'QCR_holdings') {
        if (doc.document && doc.document._source) {
            link = doc.document._source.link;
        }
    } else if (doc.doc_type === 'tweet' && doc.document.id_str) {
        link = 'http://twitter.com/' + doc.document.user.screen_name + '/statuses/' + doc.document.id_str;
    }
    $('#document-link span').empty();
    if (link) {
        $('#document-link span').append($('<a/>').attr(
            {href: link, target: '_blank'}).text(link));
        $('#document-link').show();
    } else {
        $('#document-link').hide();
    }
    actionState.currentDocument = id;
    actionData.lineupdocrow = row;
    actionData.lineupdoc = doc;
    if (!actionState.documents[id]) {
        actionState.documents[id] = {
            'derog': null
        };
    }
    if (response === null) {
        docrankApp.logState('selectDocument');
    }
    $('#document-posted,#document-geo').css('display', 'none');
    var info = {};
    if (response && response.document) {
        info = response.document;
        if (info.posted) {
            $('#document-posted').text(info.posted).css('display', '');
        }
        if (info.html) {
            $('#document-html').html(info.html);
            $('#document-html a').attr('target', '_blank');
        }
        if (info.latitude !== undefined && info.longitude !== undefined) {
            $('#document-geo .latitude').html(info.latitude >= 0 ? info.latitude + '&deg;N' : '' + (-info.latitude) + '&deg;S');
            $('#document-geo .longitude').html(info.longitude >= 0 ? info.longitude + '&deg;E' : '' + (-info.longitude) + '&degW');
            $('#document-geo a').attr('href', 'http://www.google.com/maps/@' + info.latitude + ',' + info.longitude + ',15z');
            $('#document-geo').css('display', '');
        }
        if (info.type) {
            $('#document-type span').text(info.type);
        }
    } else {
        var doc_type = doc.doc_type || 'Unknown';
        if (doc_type === 'QCR_holdings' || doc_type === 'tweet') {
            doc_type = 'Tweet';
        }
        $('#document-type span').text(doc_type);
        $('#document-html').text(row.orig_description || row.description || '');
    }
    if (info.mt_text) {
        $('#document-mt-text span').text(info.mt_text);
        $('#document-mt-text').show();
    } else {
        $('#document-mt-text').hide();
        $('#document-mt-text span').text('');
    }
    if (info.image) {
        $('#document-image').css('background-image', 'url(' + info.image + ')').attr('srcurl', info.image);
        $('#document-image').show();
    } else {
        $('#document-image').css('background-image', 'url(blank.png)');
        $('#document-image').hide();
    }
    pairedCheckbox('document-derog', 'set', actionState.documents[id].derog);
    interfaceShowHide('document-panel', 'docuser-panel');
}

/* Select the next item that needs to be examined.  If lineup is showing, this
 * will go through the rows in lineup.
 *
 * @param step: if -1, step backwards.  If 1 or undefined, step forward.  If
 *              'match', step foward unless on the last item, in which case
 *              show the next match.
 */
function selectItemNext(step) {
    if ($('.modal').is(':visible')) {
        return;
    }
    var key, cursel;
    var items = $('#doc-match-rankings:visible');
    if (items.is('.selected')) {
        cursel = items.index(items.filter('.selected'));
    }
    var lineupdata = {};
    _.each($('.lu-wrapper:visible'), function (elem) {
        var key = $(elem).parent().attr('lineup-key');
        lineupdata[key] = {
            scrollTop: $(elem).prop('scrollTop'),
            scrollHeight: $(elem).prop('scrollTop'),
            height: $(elem).height(),
            rowHeight: lineup[key].config.svgLayout.rowHeight,
            elem: $(elem)
        };
        var lineupsel = lineup[key].storage.selectedRows();
        _.each(lineup[key].spec.dataspec.data, function (row, idx) {
            if (lineupsel.length && lineupsel[0] === row) {
                cursel = items.length;
            }
            items.push({'type': 'lineup', key: key, pos: idx});
        });
    });
    if (!items.length) {
        return;
    }
    if (step === 'match' && cursel === items.length - 1) {
        if ($('#doc-next-button').is(':visible')) {
            reviewNext();
            return;
        }
    }
    step = parseInt(step);
    if (isNaN(step)) {
        step = 1;
    }
    step = (step === -1 ? items.length - 1 : (step || 1));
    var sel = (cursel === undefined ? 0 : ((cursel + step) % items.length));
    if (items[sel].type === 'lineup') {
        key = items[sel].key;
        var row = items[sel].pos;
        var ypos = row * lineupdata[key].rowHeight;
        if (ypos < lineupdata[key].scrollTop) {
            lineupdata[key].elem[0].scrollTop = ypos;
            lineup[key].updateBody();
        } else if (ypos > lineupdata[key].scrollTop + lineupdata[key].height - lineupdata[key].rowHeight) {
            lineupdata[key].elem[0].scrollTop = ypos - lineupdata[key].height + lineupdata[key].rowHeight;
            lineup[key].updateBody();
        }
        lineup[key].select(lineup[key].spec.dataspec.data[items[sel].pos]);
        lineup[key].listeners.selected(lineup[key].spec.dataspec.data[items[sel].pos]);
    } else {
        sel = $(items[sel]);
        sel.trigger('click');
    }
}

/* Select the No side of a paired checkbox.
 */
function selectItemNo() {
    selectItemPaired('n');
}

/* Use the number keys to adjust confidence.
 *
 * @param evt: the event that triggered this call.
 */
function selectItemNumber(evt) {
    var num = evt.which - 48;
    console.log(num, $('.modal').is(':visible'), $('#match-confidence-ctl').is(':visible'));
    if (num < 1 || num > 9 || $('.modal').is(':visible') || !$('#match-confidence-ctl').is(':visible')) {
        return;
    }
    var val = Math.round(100 * (num - 1) / 8);
    $('#match-confidence').bootstrapSlider('setValue', val).trigger('change');
}

/* Select a paired checkbox.
 *
 * @param: side to select: 'y', 'n', or '-'.
 */
function selectItemPaired(side) {
    if ($('.modal').is(':visible')) {
        return;
    }
    var checkboxes = $('.selected .paired-checkbox:visible');
    if (!checkboxes.length) {
        checkboxes = $('.paired-checkbox:visible').slice(-2);
    }
    if (!checkboxes.length) {
        return;
    }
    _.each(checkboxes, function (elem) {
        var id = $(elem).attr('id');
        if (id && id.substr(id.length - 1) === side && !$(elem).prop('checked')) {
            $(elem).trigger('click');
        } else if (side === '-' && $(elem).prop('checked')) {
            $(elem).trigger('click');
        }
    });
    selectItemNext('match');
}

/* Select the previous item that needs to be examined.  If lineup is showing,
 * this will go through the rows in lineup. */
function selectItemPrevious() {
    selectItemNext(-1);
}

/* Select the Yes side of a paired checkbox.
 */
function selectItemYes() {
    selectItemPaired('y');
}

/* Select a row in lineup.
 *
 * @param row: the row of data we passed to lineup.
 */
function selectUserFromLineup(row) {
    logSelectLineUpEntry();

    if (!row || !row.id) {
        return;
    }
    var handle  = $('#person-list select').val();
    if (handle.indexOf(' - ') >= 0) {
        handle = handle.substr(0, handle.indexOf(' - '));
    }
    selectUserFromLineupProcess(null, row);
    docrankApp.ajax('getpotentialuser', 'replace', {
        url: 'service/getpotentialuser/' + encodeURIComponent(handle) + '/' + encodeURIComponent(row.id),
        dataType: 'json',
        success: selectUserFromLineupProcess
    });
}

/* Update the right panel with information about the selected user.  This is
 * expected to be called twice: once upon first selection with data we already
 * have, and once when additional data is returned from an ajax call.
 *
 * @param response: response from the ajax call or null to indicate this is
 *                  called with row data.
 * @param row: if response is null, the known row data.
 */
function selectUserFromLineupProcess(response, row) {
    var match = row;
    if (response !== null) {
        row = actionData.lineuprow;
        match = response.result || row;
    }
    $('#match #match-type span').text(match.type || 'Unknown');
    $('#match #match-id span').text(match.user_id || 'Unknown');
    $('#match #match-description span').text(match.description || '');
    var id = row.id;
    var link;
    if (match.type === 'twitter_user') {
        link = 'http://twitter.com/' + match.user_id;
    }
    $('#match #match-link span').empty();
    if (link) {
        $('#match #match-link span').append($('<a/>').attr(
            {href: link, target: '_blank'}).text(link));
        $('#match #match-link').show();
    } else {
        $('#match #match-link').hide();
    }
    if (match.image) {
        $('#match-image').css('background-image', 'url(' + match.image + ')').attr('srcurl', match.image);
        $('#match-image').show();
    } else {
        $('#match-image').css('background-image', 'url(blank.png)');
        $('#match-image').hide();
    }
    showPlaceComparison(response);
    actionState.currentMatch = id;
    actionData.lineuprow = row;
    actionData.lineupmatch = match;
    if (!actionState.matches[id]) {
        actionState.matches[id] = {
            check: null,
            confidence: 50,
            derog: null
        };
    }
    if (response === null) {
        docrankApp.logState('selectUser');
    }
    pairedCheckbox('match-enabled', 'set', actionState.matches[id].check);
    $('#match-confidence').bootstrapSlider(
        'setValue', actionState.matches[id].confidence);
    interfaceShowHide('match-panel');
}

/* If a response from getpotentialuser includes place comparison information,
 * show a table of that data.
 *
 * @param: response: the response from the ajax call.
 */
function showPlaceComparison(response) {
    if (!response || !response.places || !response.entity_places) {
        $('#match-place-comp').empty().css('display', 'none');
        return;
    }
    $('#match-place-comp').html(docrankApp.templates.placetable({data: response}));
    $('#match-place-comp').css('display', 'block');
    infoTableHeight('#match-place-comp .placetable');
}

/* Show the currently selected match, next and previous buttons as appropriate,
 * and fetch the lineup data for this set of documents.
 */
function updateDocumentLineup() {
    /* we may want to disable y/n keyboard activity until the data is present
     */
    var row = actionData.matchedRows[actionState.currentRow];
    var match = actionState.matches[row.id];
    $('#doc-match-desc').text(row.row.description);
    $('#doc-match-confidence').text(row.match.confidence || 0);
    $('#doc-user-metrics').empty();
    pairedCheckbox('doc-user-derog', 'set', match.derog);
    interfaceShowHide(null, ['document-panel', 'docuser-panel']);
    docrankApp.ajax(['lineupuserdoc', 'getuserrankings'], 'cancel');
    docrankApp.ajax('getuserrankings', 'replace', {
        url: 'service/getuserrankings/' + encodeURIComponent(actionState.guid) + '/' + encodeURIComponent(row.id),
        dataType: 'json',
        success: function (response) {
            var list = response.layout.primary[1].children,
                col = response.columns;
            for (var i = 0; i < list.length; i += 1) {
                var key = list[i].column;
                for (var c = 0; c < col.length; c += 1) {
                    if (col[c].column === key) {
                        var info = col[c], name = info.label || key;
                        var val = response.result[0][key];
                        if (val !== undefined && info !== undefined) {
                            var elem = $('<span class="user-metric"><span class="user-metric-label"></span><span class="user-metric-value"></span></span>');
                            $('.user-metric-label', elem).text(name);
                            $('.user-metric-value', elem).text(val);
                            $('#doc-user-metrics').append(elem);
                        }
                    }
                }
            }
        }
    });
    docrankApp.ajax('lineupuserdoc', 'replace', {
        url: 'service/lineupuserdoc/' + encodeURIComponent(actionState.guid) + '/' + encodeURIComponent(row.id),
        dataType: 'json',
        success: function (response) {
            var count = 0;
            _.each(['original', 'repeat', 'mention'], function (key) {
                if (!response.result[key]) {
                    $('#lugui-container-' + key).addClass('nolineup').removeClass(
                        'onelineup').removeClass('twolineup');
                    return;
                }
                $('#lugui-container-' + key).removeClass('nolineup');
                var dataset = response.result[key].result,
                    desc = response.result[key];
                for (var i = 0; i < dataset.length; i += 1) {
                    var row = dataset[i];
                    if (actionState.documents[row.id]) {
                        row.derog = pairedCheckboxText(
                            actionState.documents[row.id].derog);
                    }
                }
                lineup[key] = createLineup(
                    '#lugui-wrapper-' + key, key, desc, dataset, lineup[key],
                    'Combined');
                lineup[key].on('selected', selectDocumentFromLineup);
                count += 1;
            });
            $('.lugui-doc-container').not('.nolineup').toggleClass(
                'onelineup', count === 1).toggleClass(
                'twolineup', count === 2);
            /* Fill in the user information */
            var match = response.entity;
            $('#docuser-type span').text(match.type || 'Unknown');
            $('#docuser-id span').text(match.user_id || 'Unknown');
            if (match.description && (match.description.indexOf('<a href="') >= 0 || match.description.indexOf('<br') >= 0)) {
                $('#docuser-details').html(match.description).removeClass('nolinks');
                $('#docuser-details a').attr({target: '_blank'});
            } else {
                $('#docuser-details').text(match.description || '').addClass('nolinks');
            }
            var link;
            if (match.type === 'twitter_user') {
                link = 'http://twitter.com/' + match.user_id;
            }
            $('#docuser-link span').empty();
            if (link) {
                $('#docuser-link span').append($('<a/>').attr(
                    {href: link, target: '_blank'}).text(link));
                $('#docuser-link').show();
            } else {
                $('#docuser-link').hide();
            }
            if (match.image) {
                $('#docuser-image').css('background-image', 'url(' + match.image + ')').attr('srcurl', match.image);
                $('#docuser-image').show();
            } else {
                $('#docuser-image').css('background-image', 'url(blank.png)');
                $('#docuser-image').hide();
            }
            /* Automatically select the match user */
            selectDocMatch();
        }
    });
}

/* Get the current document options and store them for the current document.
 */
function updateDocumentOptions() {
    var documentState = actionState.documents[actionState.currentDocument];
    documentState.derog = pairedCheckbox('document-derog');
    var enabledText = pairedCheckboxText(documentState.derog);
    actionData.lineupdocrow.derog = enabledText;
    var key = actionData.lineupdocrow.source || 'original';
    lineup[key].updateBody();
    docrankApp.logState('updateDocumentOptions');
}

/* Get the current document user options and store them for the current
 * potential match.
 */
function updateDocumentUserOptions() {
    var row = actionData.matchedRows[actionState.currentRow];
    var match = actionState.matches[row.id];
    match.derog = pairedCheckbox('doc-user-derog');
    docrankApp.logState('updateDocumentUserOptions');
}

/* Get the current match options and store them for the current match so that
 * we can work with them later.
 *
 * @param evt: a triggering event.  null if not from an event.
 * @param row: the row to adjust.  If not specified, use the selected lineup
 *             row.
 * @param matchState: the state of the current match.  If not specified, get
 *                    this from the match controls.
 * @param update: if not false, update lineup.
 */
function updateMatchOptions(evt, row, matchState, update) {
    row = row || actionData.lineuprow;
    if (evt && !matchState) {
        matchState = actionState.matches[actionState.currentMatch];
        matchState.check = pairedCheckbox('match-enabled');
        matchState.confidence = $('#match-confidence').bootstrapSlider(
            'getValue');
        docrankApp.logState('updateMatchOptions');
    }
    row.enabled = pairedCheckboxText(matchState.check);
    row.confidence = row.enabled !== '' ? matchState.confidence : undefined;
    if (update !== false) {
        lineup.main.updateBody();
    }
}

/* Get the current setting of the cases control and populate the person list
 * based on it.
 *
 * @param evt: event that triggered this or null for not an event.
 * @param selectFirst: if true, select and examine the first person in the
 *                     list.
 */
function updatePersonList(evt, selectFirst) {
    var casename = $('#ga-name').val();
    if (casename.endsWith(' *')) {
        casename = casename.substr(0, casename.length - 2);
    }
    if ($('#person-list').attr('case') === casename) {
        return;
    }
    actionState.case = casename;
    docrankApp.logState('selectCase');
    /* Clear old details */
    interfaceShowHide(null, ['document-controls', 'document-panel', 'docuser-panel', 'match-controls', 'match-panel']);
    $('#graph1-json-info').empty();
    $('#person-list').attr('case', casename);
    delete actionState.guid;
    $('#person-list select').empty();
    docrankApp.ajax('updatepersonlist', 'replace', {
        url: 'service/getcasenames/' + casename,
        dataType: 'json',
        success: function (response) {
            var elem = $('#person-list select');
            var res = response.result;
            if (!res || !res.order) {
                return;
            }
            for (var i = 0; i < res.order.length; i += 1) {
                var guid = res.order[i];
                var text = res.guids[guid];
                if (text.indexOf(' - ') >= 0) {
                    text = text.substr(text.indexOf(' - ') + 3);
                }
                var opt = $('<option>').attr({
                    value: guid,
                    title: text
                }).text(text);
                if (!res.used[guid]) {
                    opt.addClass('no-documents');
                }
                elem.append(opt);
            }
            if (selectFirst) {
                $('#person-list select option').eq(0).prop('selected', true);
                examinePerson();
            }
        }
    });
}

// update the user list from a response
function updateUserList(namelist, allnamelist) {
    $('#ga-name').autocomplete({
        source: allnamelist,
        delay: 300,
        minLength: 0,
        select: function () {
            window.setTimeout(updatePersonList, 1);
        },
        change: updatePersonList
    });
}

/* Initialize when the window is ready. */
$(function () {
    $('#ga-name').autocomplete();

    docrankApp.ajax('defaults', 'replace', {
        url: 'defaults.json',
        dataType: 'json',
        success: function (defaults) {
            defaults = defaults || {};

            for (var key in defaults) {
                if (defaults.hasOwnProperty(key)) {
                    docrankApp[key] = defaults[key];
                }
            }

            // set up the keystroke and mouse logger
            initializeLoggingFramework(defaults);

            $('#examine-button').on('click', examinePerson);
            $('#review-button').on('click', reviewDocuments);
            $('#doc-prev-button').on('click', reviewPrevious);
            $('#doc-next-button').on('click', reviewNext);
            $('.paired-checkbox').on('change', pairedCheckboxChanged);
            $('#doc-report-button').on('click', generateReport);
            $('#match-confidence').bootstrapSlider();
            $('#match-confidence').on('change', updateMatchOptions);
            $('#match-enabled-n,#match-enabled-y').on(
                'change', updateMatchOptions);
            $('#document-derog-n,#document-derog-y').on(
                'change', updateDocumentOptions);
            $('#doc-user-derog-n,#doc-user-derog-y').on(
                'change', updateDocumentUserOptions);
            $('#doc-match-rankings').on('click', selectDocMatch);
            docrankApp.router.on('route:stateref', restoreState);
            /* keyboard bindings */
            Mousetrap.bind('c', function () {
                if (!$('.modal').is(':visible')) {
                    $('#ga-name').focus();
                    return false;
                }
            });
            Mousetrap.bind('C', nextCase);
            Mousetrap.bind('p', function () {
                if (!$('.modal').is(':visible')) {
                    $('#person-list select').focus();
                }
            });
            Mousetrap.bind('x', examinePerson);
            Mousetrap.bind('X', nextPerson);
            Mousetrap.bind('r', reviewDocuments);
            Mousetrap.bind(',', selectItemPrevious);
            Mousetrap.bind('.', selectItemNext);
            Mousetrap.bind('y', selectItemYes);
            Mousetrap.bind('n', selectItemNo);
            Mousetrap.bind(['1', '2', '3', '4', '5', '6', '7', '8', '9'],
                           selectItemNumber);
            Mousetrap.bind('<', reviewPrevious);
            Mousetrap.bind('>', reviewNext);
            Mousetrap.bind('g', generateReport);
            /* We should have a session value from userale; we may want to
             * change this to our own session value. */
            var query = docrankApp.parseQueryString(location.search);
            var userkeys = ['USID', 'usid', 'user'];
            _.each(userkeys, function (userkey) {
                if (query[userkey]) {
                    actionState.session = query[userkey];
                } else if ($.cookie(userkey)) {
                    actionState.session = $.cookie(userkey);
                }
            });
            if (actionState.session) {
                $.cookie('USID', actionState.session);
            }
            loadCaseList();
            docrankApp.logState('started');
        }
    });
});
