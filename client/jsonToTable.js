/* global docrankApp */
'use strict';

$(function () {
    var attrOrder = [
            ['Email', 'end'],
            ['Relationship', 'equal'],
            ['Sex', 'equal'],
            ['Age', 'equal'],
            ['Address', 'start', 1, 'intpart', 'Street', 'sub'],
            ['Address', 'start', 1, 'intpart', 'City', 'sub'],
            ['Address', 'start', 1, 'intpart', 'State', 'sub'],
            ['Address', 'start', 1, 'intpart', 'Country', 'sub'],
            ['Citizenship', 'equal'],
            ['Country Of Birth', 'equal'],
            ['Full Place Of Birth', 'equal']
        ],
        attrOrderMaxDepth = 3,
        nextToggleId = 1;

    function jsonToTable(spec) {
        var that = {},
            table = $('<table>').addClass('jsontable'),
            rows = [],
            data = spec.data || [],
            parent = spec.parent,
            specialCases = spec.specialCases || {},
            keyTransform = spec.keyTransform || function (k) {
                return k;
            };

        that.outputRow = function (key, value) {
            rows.push([keyTransform(key), value]);
        };

        that.renderRow = function (key, value) {
            var row = $('<tr>'),
                k = $('<td>'),
                v = $('<td>');
            k.text(key);
            k.appendTo(row);
            v.text(value);
            v.appendTo(row);
            row.appendTo(table);
        };

        that.render = function () {
            var topRows;

            for (topRows = 0; topRows < rows.length; topRows += 1) {
                if (scoreRow(rows[topRows][0], attrOrderMaxDepth) >= attrOrder.length) {
                    break;
                }
            }
            if (!topRows || topRows === rows.length) {
                topRows = rows.length + 1;
            }
            var toggleid = 'jsontable_hidden_' + nextToggleId;
            nextToggleId += 1;
            $.each(rows, function (i, row) {
                if (i === topRows) {
                    table.append($('<tr>').append($('<td>').attr('colspan', 2)).attr({'data-toggle': 'collapse', 'data-target': '.' + toggleid}).addClass('accordion-toggle jsontable_accordion'));
                }
                that.renderRow(row[0], row[1]);
                if (i >= topRows) {
                    $('tr', table).last().addClass(toggleid).addClass('accordion-body collapse jsontable_hidden');
                }
            });
        };

        that.traverseArray = function (prefix, arr) {
            $.each(arr, function (i, value) {
                that.traverse(prefix + ' ' + (i + 1), value);
            });
        };

        that.traverseObject = function (prefix, obj) {
            var nesting = true;
            $.each(obj, function (key, value) {
                if ($.isPlainObject(value) || $.isArray(value)) {
                    nesting = true;
                }
            });
            if (nesting) {
                $.each(obj, function (key, value) {
                    if (specialCases[key] === null) {
                        key = key; // Output nothing
                    } else if (specialCases[key]) {
                        specialCases[key].call(that, prefix, value);
                    } else {
                        that.traverse(prefix + ' ' + key, value);
                    }
                });
            } else {
                var combinedValue = '';
                $.each(obj, function (key, value) {
                    combinedValue += ' ' + value;
                });
                that.outputRow(prefix, combinedValue);
            }
        };

        that.traverse = function (prefix, value) {
            if ($.isPlainObject(value)) {
                that.traverseObject(prefix, value);
            } else if ($.isArray(value)) {
                that.traverseArray(prefix, value);
            } else {
                that.outputRow(prefix, value);
            }
        };

        that.traverse('', data);

        rows.sort(function (a, b) {
            for (var depth = 0; depth < attrOrderMaxDepth; depth += 1) {
                var scoreA = Math.max(scoreRow(a[0]), scoreRow(a[0], depth)),
                    scoreB = Math.max(scoreRow(b[0]), scoreRow(b[0], depth));
                if (scoreA !== scoreB) {
                    return scoreA < scoreB ? -1 : 1;
                }
            }
            if (a[0] < b[0]) {
                return -1;
            }
            if (a[0] > b[0]) {
                return 1;
            }
            return 0;
        });

        that.render();

        table.appendTo(parent);
    }

    function scoreRow(text, depth) {
        var score, i, matched, value;

        depth = depth || attrOrderMaxDepth;
        text = text.trim();
        for (score = 0; score < attrOrder.length; score += 1) {
            matched = true;
            for (i = 0; i < attrOrder[score].length && i < depth * 2; i += 2) {
                var match = attrOrder[score][i],
                    comp = attrOrder[score][i + 1];
                switch (comp) {
                    case 'equal':
                        matched = matched && (text === match);
                        break;
                    case 'start':
                        matched = matched && text.startsWith(match);
                        break;
                    case 'end':
                        matched = matched && text.endsWith(match);
                        break;
                    case 'sub':
                        matched = matched && text.indexOf(match) >= 0;
                        break;
                    case 'intpart':
                        value = parseInt(text.split(' ')[match]);
                        if (i + 2 === depth * 2 && !isNaN(value)) {
                            return value;
                        }
                        matched = matched && !isNaN(value);
                        break;
                    default:
                        matched = false;
                        break;
                }
            }
            if (matched) {
                return score;
            }
        }
        return score;
    }

    docrankApp.jsonToTable = function (elem, data) {
        jsonToTable({
            data: data,
            parent: $(elem || 'body'),
            specialCases: {
                // Turn each Payload into a key-value pair
                Payload: function (prefix, arr) {
                    var that = this;
                    $.each(arr, function (idx, value) {
                        var k = value.PayloadName || '',
                            v = value.PayloadValue || '';
                        if (k === 'DATAITEM') {
                            k = 'DATA_ITEM';
                        }
                        if (k.length > 3) {
                            var parts = k.split('_'),
                                i = 0;
                            for (i = 0; i < parts.length; i += 1) {
                                parts[i] = parts[i].charAt(0) + parts[i].slice(1).toLowerCase();
                            }
                            k = parts.join(' ');
                        }
                        that.outputRow(prefix + ' ' + k, v);
                    });
                },

                // Assuming 1 identity, so do not append 'Identity 1' to prefix
                Identity: function (prefix, arr) {
                    var that = this;
                    $.each(arr, function (i, value) {
                        that.traverse(prefix, value);
                    });
                },

                // These fields simply duplicate other data
                Domain: null,
                Username: null,
                NameType: null
            },

            // Split apart CamelCase keys into words
            keyTransform: function (key) {
                var parts = key.replace(/_/g, ' ').split(' '),
                    i;
                for (i = 0; i < parts.length; i += 1) {
                    if (parts[i] !== parts[i].toUpperCase()) {
                        parts[i] = parts[i].replace(
                            /([a-z])([A-Z])/g, '$1 $2');
                    }
                }
                return parts.join(' ');
            }
        });
    };
});
